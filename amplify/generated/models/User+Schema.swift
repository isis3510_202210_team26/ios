// swiftlint:disable all
import Amplify
import Foundation

extension User {
  // MARK: - CodingKeys 
   public enum CodingKeys: String, ModelKey {
    case id
    case name
    case email
    case bio
    case username
    case birthdate
    case age
    case profilePic
    case coverImg
    case interests
    case createdAt
    case updatedAt
  }
  
  public static let keys = CodingKeys.self
  //  MARK: - ModelSchema 
  
  public static let schema = defineSchema { model in
    let user = User.keys
    
    model.authRules = [
      rule(allow: .public, operations: [.create, .update, .delete, .read])
    ]
    
    model.pluralName = "Users"
    
    model.fields(
      .id(),
      .field(user.name, is: .required, ofType: .string),
      .field(user.email, is: .required, ofType: .string),
      .field(user.bio, is: .optional, ofType: .string),
      .field(user.username, is: .required, ofType: .string),
      .field(user.birthdate, is: .required, ofType: .string),
      .field(user.age, is: .required, ofType: .int),
      .field(user.profilePic, is: .optional, ofType: .string),
      .field(user.coverImg, is: .optional, ofType: .string),
      .field(user.interests, is: .optional, ofType: .embeddedCollection(of: String.self)),
      .field(user.createdAt, is: .optional, isReadOnly: true, ofType: .dateTime),
      .field(user.updatedAt, is: .optional, isReadOnly: true, ofType: .dateTime)
    )
    }
}