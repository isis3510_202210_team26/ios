// swiftlint:disable all
import Amplify
import Foundation

public struct Gift: Model {
  public let id: String
  public var name: String
  public var price: Int
  public var image: String
  public var description: String
  public var categories: [String?]
  public var createdAt: Temporal.DateTime?
  public var updatedAt: Temporal.DateTime?
  
  public init(id: String = UUID().uuidString,
      name: String,
      price: Int,
      image: String,
      description: String,
      categories: [String?] = []) {
    self.init(id: id,
      name: name,
      price: price,
      image: image,
      description: description,
      categories: categories,
      createdAt: nil,
      updatedAt: nil)
  }
  internal init(id: String = UUID().uuidString,
      name: String,
      price: Int,
      image: String,
      description: String,
      categories: [String?] = [],
      createdAt: Temporal.DateTime? = nil,
      updatedAt: Temporal.DateTime? = nil) {
      self.id = id
      self.name = name
      self.price = price
      self.image = image
      self.description = description
      self.categories = categories
      self.createdAt = createdAt
      self.updatedAt = updatedAt
  }
}