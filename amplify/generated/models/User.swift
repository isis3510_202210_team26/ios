// swiftlint:disable all
import Amplify
import Foundation

public struct User: Model {
  public let id: String
  public var name: String
  public var email: String
  public var bio: String?
  public var username: String
  public var birthdate: String
  public var age: Int
  public var profilePic: String?
  public var coverImg: String?
  public var interests: [String?]?
  public var createdAt: Temporal.DateTime?
  public var updatedAt: Temporal.DateTime?
  
  public init(id: String = UUID().uuidString,
      name: String,
      email: String,
      bio: String? = nil,
      username: String,
      birthdate: String,
      age: Int,
      profilePic: String? = nil,
      coverImg: String? = nil,
      interests: [String?]? = nil) {
    self.init(id: id,
      name: name,
      email: email,
      bio: bio,
      username: username,
      birthdate: birthdate,
      age: age,
      profilePic: profilePic,
      coverImg: coverImg,
      interests: interests,
      createdAt: nil,
      updatedAt: nil)
  }
  internal init(id: String = UUID().uuidString,
      name: String,
      email: String,
      bio: String? = nil,
      username: String,
      birthdate: String,
      age: Int,
      profilePic: String? = nil,
      coverImg: String? = nil,
      interests: [String?]? = nil,
      createdAt: Temporal.DateTime? = nil,
      updatedAt: Temporal.DateTime? = nil) {
      self.id = id
      self.name = name
      self.email = email
      self.bio = bio
      self.username = username
      self.birthdate = birthdate
      self.age = age
      self.profilePic = profilePic
      self.coverImg = coverImg
      self.interests = interests
      self.createdAt = createdAt
      self.updatedAt = updatedAt
  }
}