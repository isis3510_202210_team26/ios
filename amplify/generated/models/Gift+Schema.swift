// swiftlint:disable all
import Amplify
import Foundation

extension Gift {
  // MARK: - CodingKeys 
   public enum CodingKeys: String, ModelKey {
    case id
    case name
    case price
    case image
    case description
    case categories
    case createdAt
    case updatedAt
  }
  
  public static let keys = CodingKeys.self
  //  MARK: - ModelSchema 
  
  public static let schema = defineSchema { model in
    let gift = Gift.keys
    
    model.authRules = [
      rule(allow: .public, operations: [.create, .update, .delete, .read])
    ]
    
    model.pluralName = "Gifts"
    
    model.fields(
      .id(),
      .field(gift.name, is: .required, ofType: .string),
      .field(gift.price, is: .required, ofType: .int),
      .field(gift.image, is: .required, ofType: .string),
      .field(gift.description, is: .required, ofType: .string),
      .field(gift.categories, is: .required, ofType: .embeddedCollection(of: String.self)),
      .field(gift.createdAt, is: .optional, isReadOnly: true, ofType: .dateTime),
      .field(gift.updatedAt, is: .optional, isReadOnly: true, ofType: .dateTime)
    )
    }
}