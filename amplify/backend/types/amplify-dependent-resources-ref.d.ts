export type AmplifyDependentResourcesAttributes = {
    "api": {
        "gifter": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    },
    "storage": {
        "GifterUserProfileAndCover": {
            "BucketName": "string",
            "Region": "string"
        }
    }
}