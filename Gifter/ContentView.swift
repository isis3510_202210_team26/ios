//
//  ContentView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 24/03/22.
//

import Amplify
import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var sessionManager: SessionManager
    let user: AuthUser
    
    var body: some View {
        VStack{
        Text("You signed in using Amplify!!")
            .padding()
        Button("Sign Out", action: {sessionManager.signOut()})
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    
    private struct DummyUser: AuthUser {
        let userId: String = "1"
        let username: String = "dummy"
        
    }
    
    static var previews: some View {
        ContentView(user: DummyUser())
    }
}
