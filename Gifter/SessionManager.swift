//
//  SessionManager.swift
//  Gifter
//
//  Created by Sofía Álvarez on 25/03/22.
//

import Amplify
import Combine
import KeychainAccess


enum AuthState {
    case initial
    case signUp
    case login
    case categories(username: String)
    case confirmCode(username: String, email: String)
    case session(user: AuthUser)
}

struct RuntimeError: Error {
    let message: String

    init(_ message: String) {
        self.message = message
    }

    public var localizedDescription: String {
        return message
    }
}


final class SessionManager: ObservableObject {
    @Published var currentUser: User?
    @Published var authState: AuthState = .initial //By default
    @Published var errorMessage : String = ""
    let keychain = Keychain(service: "com.bestteam26.Gifter")


    // Is the user logged in ? If it is, we give him/her a session. If not, we show them the login screen.
    func getCurrentAuthUser() {
        if let user = Amplify.Auth.getCurrentUser() {
            print("USER", user)
            authState = .session(user: user)
        }
        else {
            authState = .initial
        }
    }
    
    func showInitialView() {
        authState = .initial
    }

    // Show sign up screen
    func showSignUp() {
        authState = .signUp
    }
    
    // Show log in screen
    func showLogIn() {
        authState = .login
    }
    
    // Show categories screen
    //func showCategories(username: String, confirmationCode: String) {
    func showCategories(username: String) {
        //authState = .categories(username: username, confirmationCode: confirmationCode)
        authState = .categories(username: username)
    }
    
    func showConfirmationView(username: String) {
        authState = .confirmCode(username: username, email: "your email")
    }
    
    func signUp(username: String, email: String, password: String) {
        
        
        Amplify.DataStore.start { result in
            switch result {
            case .success:
                print("DataStore started")
            case .failure(let error):
                print("Error starting DataStore: \(error)")
            }
        }
        
        let attributes = [AuthUserAttribute(.email, value: email)]
        let options = AuthSignUpRequest.Options(userAttributes: attributes)
        
        _ = Amplify.Auth.signUp(
            username: username,
            password: password,
            options: options
        ) {[weak self] result in switch result {
        case .success(let signUpResult):
            print("Sign up result:", signUpResult)
            self!.keychain[username] = password
            switch signUpResult.nextStep {
            case .done:
                print("Finished sign up")
            case .confirmUser(let details, _):
                print(details ?? "no details")

                DispatchQueue.main.async {
                    self?.authState = .confirmCode(username: username, email: email)
                }
            }
            
        case .failure(let error):
            print("Sign up error", error)
            DispatchQueue.main.async {
                self?.errorMessage = error.errorDescription
            }
        }
            
        }
    }
    func confirm(username: String, code:String) {
        /*
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
              Amplify.DataStore.stop { result in
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                  Amplify.DataStore.start {
                    print("-> started \($0)")
                  }
                }
              }
            }*/
        _ = Amplify.Auth.confirmSignUp(for: username,confirmationCode: code) {
            [weak self] result in
            switch result {
            case .success(let confirmResult):
                print(confirmResult)
                if confirmResult.isSignupComplete {
                    DispatchQueue.main.async {
                        self?.showCategories(username: username)
                        //self?.showLogIn()
                    }
                }
            
            case .failure(let error):
                print("failed to confirm code:", error)
                DispatchQueue.main.async {
                    self?.errorMessage = error.errorDescription
                }
            }
            
        }
    }
    
    func login(username: String, password: String) {
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
              Amplify.DataStore.stop { result in
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                  Amplify.DataStore.start {
                    print("-> started \($0)")
                  }
                }
              }
            }
        
        _ = Amplify.Auth.signIn(
            username: username,
            password: password) {
            [weak self] result in
            switch result {
            case .success(let signInResult):
                print(signInResult)
                if signInResult.isSignedIn{
                    DispatchQueue.main.async {
                        self?.getCurrentAuthUser()
                    }
                }
                else{
                    DispatchQueue.main.sync {
                    self?.errorMessage = "User is not confirmed."
                    //self?.authState = .confirmCode(username: username, email: "your email")
                    }
                }

            case .failure(let error):
                print("Login error:", error)
                DispatchQueue.main.sync {
                    self?.errorMessage = error.errorDescription
                    /*DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self?.showConfirmationView(username: username)
                    }*/
                }
            }
            
        }
        
        
    }
    
    func signOut(){
        /*
        Amplify.DataStore.clear { result in
            switch result {
            case .success:
                print("DataStore cleared")
            case .failure(let error):
                print("Error clearing DataStore: \(error)")
            }
        }*/
        
        
        _ = Amplify.Auth.signOut{[weak self] result in
            switch result {
            case .success:
                DispatchQueue.main.async {
                    self?.getCurrentAuthUser()
                }
            case .failure(let error):
                print("Sign out error:", error)
            }
            
        }
        
    }
    
    
    
}
