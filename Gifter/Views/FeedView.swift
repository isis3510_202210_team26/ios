//
//  File.swift
//  Gifter
//
//  Created by Juliana Prieto Arcila on 24/03/22.
//

import SwiftUI
import MapKit
import Amplify
import SwiftUIPullToRefresh
import WaterfallGrid


struct FeedView: View {
    @Environment(\.colorScheme) var colorScheme
    @EnvironmentObject var giftViewModel: GiftViewModel
    @EnvironmentObject var sessionManager: SessionManager
    let user: AuthUser
    let alignment = Alignment(horizontal: .leading, vertical: .top)
    let fontDefaultColor = Color("GifterTextDefault")
    var gridItemLayout = [GridItem(.flexible()), GridItem(.flexible())]
    @State var columns: Int = 2
    @State var show = false
    @State var isLoading = true
    @Namespace var animation

    var body: some View {
        NavigationView {
            RefreshableScrollView(onRefresh: { done in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                           giftViewModel.getGifts()
                          done()
                        }
                      }) {
                VStack(alignment: .leading) {
                    HStack(alignment: .center) {
                        Image("icon")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 30.0)
                        Text("Gifter")
                            .font(Font.custom("PTSerif-Bold", size: 24))
                            .foregroundColor(colorScheme == .dark ? Color.white : fontDefaultColor)
                            .padding(.top, 5.0)
                    }
                    .padding(.leading, 16.0)
                    
                    /*Color(colorScheme == .dark ? "black" : "GifterBackground")
                        .frame(height: 16)*/
                    
                    Text("Popular gift ideas")
                        .font(Font.custom("Montserrat-SemiBold", size: 22))
                        .foregroundColor(colorScheme == .dark ? Color.white : fontDefaultColor)
                        .padding(.leading, 16.0)
                        .padding(.top, 10.0)
                        .padding(.bottom, 16)
                    
   
                        StaggeredGrid(columns: columns, list: giftViewModel.gifts, content: { gift in
                            GiftView(gift: gift)
                                
                            .matchedGeometryEffect(id: gift.id, in: animation)
                                
                        }).redacted(reason: isLoading ? .placeholder : .init())
                        //.animation(Animation.easeInOut(duration: 1), value: show)
                        .padding(.horizontal)
                        .navigationTitle("Feed")
                       /* .toolbar{
                            ToolbarItem(placement: .navigationBarTrailing) {
                                Button {
                                    columns += 1
                                } label: {
                                    Image(systemName: "plus")
                                }
                            }
                            ToolbarItem(placement: .navigationBarTrailing) {
                                Button {
                                    columns = max(columns-1, 1)
                                } label: {
                                    Image(systemName: "minus")
                                }
                            }

                        }*///
                    }.animation(Animation.easeInOut, value: show)
                    
            }
            .navigationBarHidden(true)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarTitle(Text("Feed"))
            .edgesIgnoringSafeArea(.bottom)
            .onAppear{
                DispatchQueue.main.async {
                        giftViewModel.getGifts()
                        self.show.toggle()
                        isLoading = false
                    
                }
            }
        }
    }

}

struct GiftView: View {
    @Environment(\.colorScheme) var colorScheme
    var gift: GiftDTO
    var contentMode: ContentMode = .fit
    
    let alignment = Alignment(horizontal: .trailing, vertical: .top)
    
    @State private var addedToWishlist = false
    @StateObject var viewModel = WishlistViewModel()
    
    var body: some View {
        NavigationLink(destination: GiftDetailView(gift: gift)) {
            ZStack(alignment: alignment) {
                AsyncImage(url: URL(string: gift.image)){
                    phase in
                    switch phase {
                    case .empty:
                        withAnimation(.easeInOut(duration: 1)) {
                        Rectangle().fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.ui.gifterGray)
                            .cornerRadius(10)
                            .frame(minWidth: 30, minHeight: 200)
                        }
                    case .success(let image):
                        image.resizable().background(RoundedRectangle(cornerRadius: 10).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white))
                            
                            .aspectRatio(contentMode: contentMode)
                           /* .overlay(
                                LinearGradient(
                                    colors: colorScheme != .dark ? [Color(red: 0, green: 0, blue: 0, opacity: 0.3), Color(red: 0, green: 0, blue: 0, opacity: 0)] : [Color(red: 49.0/255.0, green: 49.0/255.0, blue: 49.0/255.0, opacity: 0.3) ],
                                    startPoint: .topTrailing,
                                    endPoint: .bottomLeading
                                )
                            )*/
                            
                    case .failure:
                        VStack{}
                     @unknown default:
                                        // Since the AsyncImagePhase enum isn't frozen,
                                        // we need to add this currently unused fallback
                                        // to handle any new cases that might be added
                                        // in the future:
                        VStack{}
                    }
                }
                   
                    
                    .cornerRadius(10)
                   .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.1), radius: 5, x: 0, y: 2)
                
                addToWishlistButtonView(addedToWishlist: $addedToWishlist)
            }
        }
    }
}


struct GiftCardView: View {
    @Environment(\.colorScheme) var colorScheme
    @State private var addedToWishlist = false
    var gift: GiftDTO
    var body: some View {
        AsyncImage(url: URL(string: gift.image)){
            phase in
            switch phase {
            case .empty:
                Rectangle().fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.ui.gifterGray)
                    .cornerRadius(10)
            case .success(let image):
                image.resizable()//.background(RoundedRectangle(cornerRadius: 10).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white))
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(10)

                    
            case .failure:
                VStack{}
             @unknown default:
                                // Since the AsyncImagePhase enum isn't frozen,
                                // we need to add this currently unused fallback
                                // to handle any new cases that might be added
                                // in the future:
                VStack{}
            }
            
        }
        
    }
}

struct addToWishlistButtonView: View {
    @Binding var addedToWishlist: Bool

    var body: some View {
        Button {
            self.addedToWishlist.toggle()
        } label: {
            Image("favorite")
                .resizable()
                .frame(width: 30.0, height: 30.0)
                .padding([.top, .trailing], 8)
                .foregroundColor(addedToWishlist ? Color("GifterRed") : Color("GifterGray"))
        }
    }
}

struct WaterfallGiftGrid: View {
    @EnvironmentObject var giftViewModel: GiftViewModel
    var scrollingDirection: Axis.Set = .vertical
    var columns: Int = 2
    var body: some View {
        WaterfallGrid(giftViewModel.gifts) { gift in
            GiftView(gift: gift)
        }
        .gridStyle(columns: columns, animation: .easeInOut(duration: 0.5))
        .padding([.leading, .trailing], 16)
        .scrollOptions(direction: scrollingDirection)
    }
}
