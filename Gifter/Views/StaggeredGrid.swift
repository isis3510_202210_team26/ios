//
//  StaggeredGrid.swift
//  Gifter
//
//  Created by Sofía Álvarez on 20/04/22.
//

import SwiftUI

struct StaggeredGrid<Content: View, T: Identifiable>: View where T:Hashable {
    
    var content: (T) -> Content
    var list: [T]
    
    //Columns
    var columns: Int
    
    // Properties
    var showsIndicators: Bool
    var spacing: CGFloat
    
    init(columns: Int, showsIndicators: Bool = false, spacing:CGFloat = 8, list: [T], @ViewBuilder content: @escaping (T)->Content) {
        self.content = content
        self.list = list
        self.spacing = spacing
        self.showsIndicators = showsIndicators
        self.columns = columns
    }
    
    // Staggered grid function
    func setUpList()->[[T]]{
        // Creating empty subarrays of columns
        var gridArray: [[T]] = Array(repeating: [], count: columns)
        // Splitting array for VStack oriented view
        var currentIndex: Int = 0
        for object in list {
            gridArray[currentIndex].append(object)
            // increase index count and reset if overbounds column count
            if currentIndex == (columns - 1) {
                currentIndex = 0
            }
            else {
                currentIndex += 1
            }
        }
        return gridArray
    }
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: showsIndicators) {
            HStack(alignment: .top) {
                ForEach(setUpList(), id: \.self){columnsData in
                    LazyVStack(spacing: spacing) {
                        ForEach(columnsData){ object in
                            content(object)
                        }
                    }
                }
            }
        }.padding(.vertical)
    }
}

/*
struct StaggeredGrid_Previews: PreviewProvider {
    static var previews: some View {
        StaggeredGrid()
    }
}
*/
