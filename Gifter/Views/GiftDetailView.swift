//
//  GiftDetail.swift
//  Gifter
//
//  Created by Juliana Prieto Arcila on 11/05/22.
//

import SwiftUI

struct GiftDetailView: View {
    let gift: GiftDTO
    @Environment(\.colorScheme) var colorScheme
    @State private var showFullDescription = false
    
    var body: some View {
        ScrollView {
            VStack {
                VStack(alignment: .leading) {
                    AsyncImage(url: URL(string: gift.image)){
                        phase in
                        switch phase {
                        case .empty:
                            withAnimation(.easeInOut(duration: 1)) {
                            Rectangle().fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.ui.gifterGray)
                                .frame(maxWidth: .infinity)
                                .frame(height: 350)
                            }
                        case .success(let image):
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .scaledToFill()
                                .frame(maxWidth: .infinity, maxHeight: 350)
                                .clipped()
                                .background(RoundedRectangle(cornerRadius: 10).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white))
                                .overlay(
                                    LinearGradient(
                                        colors: [Color(red: 255, green: 255, blue: 255, opacity: 0.4), Color(red: 255, green: 255, blue: 255, opacity: 0)],
                                        startPoint: .top,
                                        endPoint: .bottom
                                    )
                                )
                                
                        case .failure:
                            ZStack {}
                         @unknown default:
                            // Since the AsyncImagePhase enum isn't frozen,
                            // we need to add this currently unused fallback
                            // to handle any new cases that might be added
                            // in the future:
                            ZStack {}
                        }
                    }
                    HStack {
                        ForEach(gift.categories, id: \.self) { category in
                            Text(category)
                                .font(Font.custom("WorkSans-Regular", size: 16))
                                .foregroundColor(.white)
                                .padding([.leading, .trailing], 12)
                                .padding([.top, .bottom], 6)
                                .background(Color("GifterTurquoise"))
                                .cornerRadius(20)
                        }
                    }
                    .padding([.top, .leading], 14)
                    
                    VStack(alignment: .leading) {
                        Text(gift.name)
                            .font(Font.custom("Montserrat-SemiBold", size: 24))
                            .foregroundColor(Color("GifterTextDefault"))
                            .padding(.bottom, 0.5)
                        
                        Text(gift.description)
                            .font(Font.custom("WorkSans-Regular", size: 16))
                            .foregroundColor(Color("GifterTextDefault"))
                            .padding(.bottom, 4)
                            .lineLimit(showFullDescription ? 100 : 2)
                        
                        HStack {
                            Spacer()
                            Text(showFullDescription ? "Hide full description" : "See full description")
                                .font(Font.custom("Montserrat-SemiBold", size: 14))
                                .foregroundColor(Color("GifterRed"))
                        }
                        .onTapGesture {
                            showFullDescription = !showFullDescription
                        }
                        
                        Text("$\(gift.price)")
                            .font(Font.custom("Montserrat-Regular", size: 24))
                            .foregroundColor(Color("GifterTextDefault"))
                            .padding([.leading, .trailing], 12)
                            .padding([.top, .bottom], 8)
                            .background(Color("GifterBackground"))
                            .cornerRadius(20)
                    }
                    .padding([.leading, .trailing, .top, .bottom], 16)
                }
                Rectangle()
                    .fill(Color("GifterBackground"))
                    .frame(height: 16)
                
                VStack {
                    Text("Stores that sell this gift")
                        .font(Font.custom("Montserrat-SemiBold", size: 22))
                        .foregroundColor(Color("GifterTextDefault"))
                        .padding(.leading, 16.0)
                        .padding(.top, 10.0)
                        .padding(.bottom, 16)
                    
                    StaggeredGrid(scrollingDirection: .horizonal, columns: 1, list: [gift], content: { gift in
                        GiftView(gift: gift)
                        
                            
                        .matchedGeometryEffect(id: gift.id, in: animation)
                            
                    }).redacted(reason: isLoading ? .placeholder : .init())
                    
                    
                }
                .padding([.leading, .trailing, .top], 16)
            }
        }
    }
}

struct GiftDetailView_Previews: PreviewProvider {
    static var previews: some View {
        GiftDetailView(gift: GiftDTO(
            id: "",
            name: "",
            price: 0,
            image: "",
            description: "",
            categories: []
        ))
    }
}

struct StoreCard: View {
    @Environment(\.colorScheme) var colorScheme
    let imageName: String
    let storeName: String
    let storeDescription: String

    var body: some View {
//        NavigationLink(destination: StoreDetailView(store)) {
//                VStack(alignment: .leading) {
//                    Image(imageName)
//                        .resizable()
//                        .aspectRatio(contentMode: .fill)
//                        .frame(width: 250.0, height: 120.0)
//                        .clipped()
//
//                    Text(storeName)
//                        .font(Font.custom("Montserrat-SemiBold", size: 18))
//                        .foregroundColor(colorScheme == .dark ? Color.white : Color("GifterTextDefault"))
//                        .padding(.leading, 10)
//                        .padding(.bottom, 0.5)
//
//                    Text(storeDescription)
//                        .font(Font.custom("WorkSans-Regular", size: 14))
//                        .foregroundColor(Color("GifterText80"))
//                        .padding(.leading, 10)
//                        .padding(.bottom, 16)
//                }
//                .background(RoundedRectangle(cornerRadius: 10).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white))
//                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.12), radius: 12, x: 0, y: 2)
//                .padding(.bottom, 24.0)
//                .padding([.top, .leading], 16.0)
//        }
        ZStack {
            
        }
    }
}
