import SwiftUI

struct SearchBar: View {
    @State var searchText: String = ""
    @EnvironmentObject var userViewModel: UserViewModel

    var body: some View {
        NavigationView {
                    List {
                        ForEach(searchResults, id: \.self) { name in
                            NavigationLink(destination: Text(name)) {
                                Text(name)
                            }
                        }
                    }
                    .searchable(text: $searchText, prompt: "Search category...")
        }
    }
    var searchResults: [String] {
        if searchText.isEmpty {
            return []
        } else {
            return userViewModel.interests.filter { $0.contains(searchText) }
        }
    }
}


