//
//  StoresView.swift
//  Gifter
//
//  Created by Juliana Prieto Arcila on 20/04/22.
//

import SwiftUI
import SwiftUI
import WaterfallGrid
import MapKit
import Amplify
import SwiftUIPullToRefresh

struct StoresView: View {
    let user: AuthUser
    let fontDefaultColor = Color("GifterTextDefault")
    @Environment(\.colorScheme) var colorScheme
    @EnvironmentObject var storeViewModel: StoreViewModel
    @StateObject private var storeMapViewModel = StoreMapViewModel()
    
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: .leading) {
                    VStack {
                        HStack {
                            Image("icon")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 30.0)
                            Text("Gifter")
                                .font(Font.custom("PTSerif-Bold", size: 24))
                                .foregroundColor(colorScheme == .dark ? Color.white : fontDefaultColor)
                                .padding(.top, 5.0)
                        }
                        
                        Text("Take a look at gift shops near you")
                            .font(Font.custom("Montserrat-SemiBold", size: 18))
                            .foregroundColor(colorScheme == .dark ? Color.white : fontDefaultColor)
                            .padding(.bottom, 16.0)
                            .padding(.top, 4)
                    }.padding([.leading, .trailing], 16)
                    
                    
                    ForEach (storeViewModel.stores) { store in
                        StoreItem(store: store)
                    }
                    
                    Spacer()
                }

                .navigationBarHidden(true)
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarTitle(Text("Stores"))
            }
        }
        .onChange(of: storeMapViewModel.currentUserLocation) { newLocation in
            storeViewModel.getNearbyStores(location: newLocation.location)
        }
        .onAppear {
            storeMapViewModel.checkIfLocationServicesIsEnabled()
            storeViewModel.getNearbyStores(location: storeMapViewModel.currentUserLocation.location)
            print("Stores \(storeViewModel.stores)")
        }
    }
}

struct StoresView_Previews: PreviewProvider {
    private struct DummyUser: AuthUser {
        let userId: String = "1"
        let username: String = "dummy"
    }
    
    static var previews: some View {
        StoresView(user: DummyUser())
    }
}

struct StoreItem: View {
    @EnvironmentObject var giftViewModel: GiftViewModel
    @Environment(\.colorScheme) var colorScheme
    
    let store: StoreDTO
    
    var body: some View {
        NavigationLink(destination: StoreDetailView(store: store)
            .environmentObject(giftViewModel)) {
            VStack(alignment: .leading) {
                HStack {
                    HStack() {
                        AsyncImage(url: URL(string: store.images)){
                            phase in
                            switch phase {
                            case .empty:
                                withAnimation(.easeInOut(duration: 1)) {
                                Rectangle().fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.ui.gifterGray)
                                        .frame(width: 72.0, height: 72.0)
                                }
                            case .success(let image):
                                image
                                    .resizable()
                                    .frame(width: 75.0, height: 75.0)
                                    .clipShape(Circle())
                                    .aspectRatio(contentMode: .fit)
                                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.12), radius: 12, x: 0, y: 2)
                                    
                            case .failure:
                                ZStack {}
                             @unknown default:
                                // Since the AsyncImagePhase enum isn't frozen,
                                // we need to add this currently unused fallback
                                // to handle any new cases that might be added
                                // in the future:
                                ZStack {}
                            }
                        }

                        
                        VStack(alignment: .leading) {
                            Text(store.name)
                                .font(Font.custom("Montserrat-SemiBold", size: 14))
                                .foregroundColor(Color("GifterTextDefault"))
                                .padding(.bottom, 2)
                            
                            Text(store.description)
                                .font(Font.custom("Montserrat-Regular", size: 12))
                                .foregroundColor(Color("GifterTextDefault"))
                                .lineLimit(2)
                                .multilineTextAlignment(.trailing)
                        }
                        .padding(.leading, 8.0)
                    }
                    
                    Spacer()
                    
                    Text("View shop")
                        .font(Font.custom("Montserrat-SemiBold", size: 12))
                        .foregroundColor(Color("GifterRed"))
                        .padding(.trailing, 8)

                }
                HStack {
                    ForEach(0...2, id:\.self) { index in
                        StoreGiftPreview(imageUrl: giftViewModel.gifts[Int.random(in: 1..<15)].image)
                            .onAppear{ giftViewModel.getGifts(); }
                    }
                }
            }
            .padding(.bottom, 24)
            .padding([.leading, .trailing], 16)
        }
    }
}

struct StoreGiftPreview: View {
    @Environment(\.colorScheme) var colorScheme
    let imageUrl: String
    
    var body: some View {
        AsyncImage(url: URL(string: imageUrl)){
            phase in
            switch phase {
            case .empty:
                withAnimation(.easeInOut(duration: 1)) {
                Rectangle().fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.ui.gifterGray)
                        .frame(width: 130.0, height: 107.0)
                }
            case .success(let image):
                image
                    .resizable()
                    .frame(width: 130.0, height: 107.0)
                    .cornerRadius(5)
                    .aspectRatio(contentMode: .fit)
                    .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.12), radius: 12, x: 0, y: 2)
                    
            case .failure:
                ZStack {}
             @unknown default:
                // Since the AsyncImagePhase enum isn't frozen,
                // we need to add this currently unused fallback
                // to handle any new cases that might be added
                // in the future:
                ZStack {}
            }
        }
    }
}

//final class StoreMapViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
//
//    @Published var coordinateRegion = MKCoordinateRegion(
//      center: CLLocationCoordinate2D(latitude: 4.652236, longitude: -74.126080),
//      span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
//
//    var locationManager: CLLocationManager?
//
//    func checkIfLocationServicesIsEnabled() {
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager = CLLocationManager()
//            locationManager!.delegate = self
//        } else {
//            print("User did not give permissions")
//        }
//    }
//
//    private func checkLocationAuthorization() {
//        guard let locationManager = locationManager else { return }
//
//        switch locationManager.authorizationStatus {
//            case .notDetermined:
//                locationManager.requestWhenInUseAuthorization()
//            case .restricted:
//                print("Location is restricted")
//            case .denied:
//                print("Location is not enabled. Please enable it")
//            case .authorizedAlways, .authorizedWhenInUse:
//            coordinateRegion = MKCoordinateRegion(
//                center: locationManager.location!.coordinate,
//                span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
//            )
//            @unknown default:
//                break
//        }
//    }
//
//    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
//        checkLocationAuthorization()
//    }
//}
