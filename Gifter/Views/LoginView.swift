//
//  LoginView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 24/03/22.
//

import SwiftUI
import Combine

struct LoginView: View {
    @EnvironmentObject var sessionManager: SessionManager
    @Environment(\.colorScheme) var colorScheme
    @State var myColorScheme: ColorScheme?
    @State private var storedColorScheme = UserDefaults.standard.string(forKey: "ColorScheme")
    @State var username: String = ""
    @State private var password: String = ""
    @State private var showingAlert = false
    @State private var thisMessageTrigger = false

    
    private func isReady(username: String, password: String) -> Bool {
        return !username.isEmpty && !password.isEmpty
    }
    
    

    var body: some View {
        HStack(spacing: 0) {
            Spacer().frame(width: 16, height: 8, alignment: .trailing)
            VStack {
                GeometryReader { geometry in
                    HStack(spacing: 0) {
                        Button {
                            sessionManager.showInitialView()
                        } label: {
                            Image("chevron_left").foregroundColor(Color("GifterRed")).scaledToFit()
                            .transition(.slide)
                        }.buttonStyle(PlainButtonStyle())
                        Text("Log in").font(Font.custom("Montserrat-SemiBold",
                                                     size: 18,
                                                     relativeTo: .title
                                                        ))
                            .fontWeight(.semibold)
                            .foregroundColor(Color("GifterRed"))
                            .multilineTextAlignment(.center)
                            .frame(maxWidth: .infinity,  alignment: .center)
                            .scaledToFit()
                        

                    }
                    VStack{
                        Spacer().frame(height: 104).scaledToFit()
                        HStack(alignment: .center){
                            Image("icon")}.frame(maxWidth: .infinity, alignment: .center)
                            .padding(.leading, 20.0)
                    Spacer().frame(width: 64, height: 64)
                        
                    TextField(
                                    "Username",
                                    text: $username
                                )
                                .padding()
                                .font(Font.custom("WorkSans-Regular",
                                                               size: 14
                                                                 ))
                                .frame(height: 40)

                                .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                .disableAutocorrection(true)
                                .onReceive(Just(username)) { inputValue in
                                            if inputValue.count > 20 || inputValue.contains(" ") {
                                                username.removeLast()
                                            }
                                        }
                    
                    Spacer().frame(width: 24, height: 24)

                    SecureField(
                                        "Password",
                                        text: $password
                                    )
                                    .padding()
                                    .font(Font.custom("WorkSans-Regular",
                                                                   size: 14
                                                                     ))
                                    .frame(height: 40)

                                    .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                    .disableAutocorrection(true)
                                    .onReceive(Just(password)) { inputValue in
                                                if inputValue.count > 20 {
                                                    password.removeLast()
                                                }
                                            }
                        GeometryReader { geometry_h in

                        VStack {
                            Spacer().frame(height: (geometry_h.size.height*0.24))
                            HStack{
                                Spacer().frame(width: geometry_h.size.width*0)
                                Button("Log In", action: {
                                    if isReady(username: username, password: password) {
                                    sessionManager.login(
                                    username: username,
                                    password: password)
                                    }
                                    else {
                                        showingAlert = true
                                    }
                                    
                                    if sessionManager.errorMessage != "" {
                                        showingAlert = true
                                        thisMessageTrigger = true
                                    }
                            })
                                .foregroundColor(.white)
                                .buttonStyle(StyledButton())
                                .background(RoundedRectangle(cornerRadius: 20, style:   .circular).fill(username.isEmpty || password.isEmpty ? Color.ui.disabledGray : Color.ui.red))
                                .font(Font.custom("WorkSans-Regular",
                                                  size: 16,
                                                  relativeTo: .title
                                                  ))
                                .alert(isPresented: $showingAlert) {
                                
                                    Alert(title: Text("Log in Error"), message: !isReady(username: username, password: password) ? Text("All fields must be complete") : sessionManager.errorMessage != "" ? Text(sessionManager.errorMessage) : Text("All fields must be complete") , dismissButton: Alert.Button.cancel(
                                        Text("OK"), action: {
                                            sessionManager.errorMessage = ""
                                            if thisMessageTrigger && isReady(username: username, password: password) {
                                                sessionManager.showConfirmationView(username: username)
                                            }
                                        }
                                    ))
                                }
                                
                            }.frame(maxWidth: .infinity, alignment: .center)
  
                        }
                        .buttonStyle(.bordered)
                        }
                    }
                    }
                
                }
            Spacer().frame(width: 24, height: 8)
            }.onTapGesture {
                let keyWindow = UIApplication.shared.connectedScenes
                                   .filter({$0.activationState == .foregroundActive})
                                   .map({$0 as? UIWindowScene})
                                   .compactMap({$0})
                                   .first?.windows
                                   .filter({$0.isKeyWindow}).first
                keyWindow!.endEditing(true)
            }
            .onAppear {
                myColorScheme = storedColorScheme == "" ? colorScheme : storedColorScheme == "dark" ? .dark : .light
            }
            .preferredColorScheme(myColorScheme ?? colorScheme)

    }
}
 

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
.previewInterfaceOrientation(.portrait)
    }
}
