//
//  InterestsView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 24/03/22.
//

import Amplify
import SwiftUI

/*
class CategoryList: ObservableObject  {
    @Published var categories: [String] = []
    //@State var numCategories: Int = categories.count
}*/

struct InterestsView: View {
    @Environment(\.colorScheme) var colorScheme
    @State var myColorScheme: ColorScheme?
    @State private var storedColorScheme = UserDefaults.standard.string(forKey: "ColorScheme")
    @EnvironmentObject var sessionManager: SessionManager
    @EnvironmentObject var userViewModel: UserViewModel
    @State private var showingAlert = false
    //@StateObject var categories = CategoryList()

    //let user: AuthUser
    let username: String
    
    @State var tapped = false
    //@State var categories = CategoryList()
    
    private func isReady() -> Bool {
        return !userViewModel.interests.isEmpty
    }
    
    var categoryTextColor: Color {
        return tapped ? Color.white: Color("GifterTurquoise")
        }

    var body: some View {
        
        HStack(spacing: 0) {
            Spacer().frame(width: 16, height: 8, alignment: .trailing)
                VStack {
                    GeometryReader { geometry in
                        HStack(spacing: 0) {
                            (Text("What are your ")
                                    + Text("interests")
                                    .foregroundColor(Color("GifterTurquoise"))
                             + Text("?")).font(Font.custom("Montserrat-SemiBold",
                                                         size: 18,
                                                         relativeTo: .title
                                                            ))
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.center)
                                .frame(maxWidth: .infinity,  alignment: .leading)
                                .scaledToFit()

                        }
                        VStack(alignment: .leading) {
                            VStack(alignment: .leading){
                                Spacer().frame(height: 40)
                                HStack{
                                    CategoryButton(tapped: self.tapped, category: "Movies, music & books", buttonWidth: 184)
                                    CategoryButton(tapped: self.tapped, category: "Home", buttonWidth: 72)
                                }
                               Spacer().frame(height: 16)
                              HStack{
                                   CategoryButton(tapped: self.tapped, category: "Kitchen & Dining", buttonWidth: 144)
                                   CategoryButton(tapped: self.tapped, category: "Musical instruments", buttonWidth: 172)
                                 }
                                Spacer().frame(height: 16)
                              HStack{
                                   CategoryButton(tapped: self.tapped, category: "Garden", buttonWidth: 82)
                                   CategoryButton(tapped: self.tapped, category: "School office supplies", buttonWidth: 184)
                                }
                              Spacer().frame(height: 16)
                                HStack{
                                    CategoryButton(tapped: self.tapped, category: "Toys", buttonWidth: 56)
                                   CategoryButton(tapped: self.tapped, category: "Women", buttonWidth: 84)
                                  }
                               }
                            GeometryReader { geometry_h in

                            VStack {
                                Spacer().frame(height: (geometry_h.size.height*0.84))
                                HStack{
                                    Spacer().frame(width: geometry_h.size.width*0)
                                Button("Continue", action: {
                                    if isReady() {
                                        sessionManager.showLogIn()
                                        userViewModel.createInterests(
                                            authUsername: username,
                                            interests: userViewModel.interests
                                    )
                                    }
                                    else{
                                        showingAlert=true
                                    }
                                    
                                })
                                    .foregroundColor(Color.white)
                                    .buttonStyle(StyledButton())
                                    .background(RoundedRectangle(cornerRadius: 20, style:   .circular).fill(userViewModel.interests.isEmpty ? Color.ui.disabledGray : Color.ui.red))
                                    .font(Font.custom("WorkSans-Regular",
                                                      size: 16,
                                                      relativeTo: .title
                                                      ))
                                    .alert(isPresented: $showingAlert) {
                                    
                                        Alert(title: Text("Error"), message: Text("You must select at least one category"), dismissButton: Alert.Button.cancel(
                                            Text("OK"), action: {
                                            }
                                        ))
                                    }
                                    
                                
                                }.frame(maxWidth: .infinity, alignment: .center)
      
                            }
                            .buttonStyle(.bordered)
                            }
                            }
                
                        }
                    }
                Spacer().frame(width: 24, height: 8)
            }.onAppear {
                myColorScheme = storedColorScheme == "" ? colorScheme : storedColorScheme == "dark" ? .dark : .light
            }
            .preferredColorScheme(myColorScheme ?? colorScheme)
        }
}

struct InterestsView_Previews: PreviewProvider {
    private struct DummyUser: AuthUser {
        let userId: String = "1"
        let username: String = "dummy"
        
    }
    static var previews: some View {
        
        InterestsView(username: "a")
    }
}


struct CategoryButton: View {
    @State var tapped: Bool
    @State var category: String
    @State var buttonWidth: CGFloat
    @EnvironmentObject var userViewModel: UserViewModel
    //@ObservedObject var cat: CategoryList
    
    var categoryTextColor: Color {
        return tapped ? Color.white: Color("GifterTurquoise")
        }
    
    var body: some View {
        Button(action: {
            tapped.toggle()
            if tapped {
                userViewModel.interests.append(category)
                //cat.categories.append(category)
            }
            else {
                userViewModel.interests = userViewModel.interests.filter { $0 != category }
                //cat.categories = cat.categories.filter { $0 != category }
            }
            print(userViewModel.interests)
            //print(cat.categories)
        }) {
            Text(category)
                .font(Font.custom("WorkSans-Regular",size: 14,
                                  relativeTo: .title))
                .foregroundColor(categoryTextColor)
                .frame(width: buttonWidth, height: 40,  alignment: .leading)
                .padding(.leading)
        }.buttonStyle(CustomButtonStyle(tapped: self.tapped))
    }
}


struct CustomButtonStyle : ButtonStyle {
    @Environment(\.colorScheme) var colorScheme
    var tapped: Bool
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .background(RoundedRectangle(cornerRadius: 20)
                            .fill(tapped ? Color("GifterTurquoise") : colorScheme == .dark ? Color.ui.darkModeGray : Color.white)
            )
            .overlay(RoundedRectangle(cornerRadius: 20)
                        .stroke(Color("GifterTurquoise")))
    }
}

