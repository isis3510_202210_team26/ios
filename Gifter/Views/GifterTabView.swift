//
//  TabView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 25/03/22.
//

import Amplify
import SwiftUI

struct GifterTabView: View {
    
    @State var nowDate: Date = Date()
    @Environment(\.colorScheme) var systemColorScheme
    @State var myColorScheme: ColorScheme?
    @State private var storedColorScheme = UserDefaults.standard.string(forKey: "ColorScheme")
    @State private var showingAlertMorning = false
    @State private var showingAlertAfternoon = false

   // let referenceDate: Date
    var timer: Timer {
        Timer.scheduledTimer(withTimeInterval: 3600, repeats: true) {_ in
            print("fired")
            let today = Date()
            let ini = Calendar.current.date(
                bySettingHour: 6, //6,
                minute: 0, //0,
                second: 0,
                of: today)!
            let fini = Calendar.current.date(
                bySettingHour: 18, //18,
                minute: 0, //0,
                second: 0,
                of: today)!
            if (Calendar.current.component(.hour, from: today) == Calendar.current.component(.hour, from: ini) ||
                Calendar.current.component(.hour, from: today) == Calendar.current.component(.hour, from: ini) - 1 ||
                Calendar.current.component(.hour, from: today) == Calendar.current.component(.hour, from: ini) + 1) &&
                //Calendar.current.component(.hour, from: today) == Calendar.current.component(.hour, from: ini) &&
                //Calendar.current.component(.minute, from: today) == Calendar.current.component(.minute, from: ini) &&
                //Calendar.current.component(.second, from: today) == Calendar.current.component(.second, from: ini) &&
                myColorScheme == .dark {
                print("MORNING! ---------------------------")
                showingAlertMorning = true
            }
            if Calendar.current.component(.hour, from: today) == Calendar.current.component(.hour, from: fini) ||
                Calendar.current.component(.hour, from: today) == Calendar.current.component(.hour, from: fini) - 1 ||
                Calendar.current.component(.hour, from: today) == Calendar.current.component(.hour, from: fini) + 1 &&
                //Calendar.current.component(.hour, from: today) == Calendar.current.component(.hour, from: fini) &&
                //Calendar.current.component(.minute, from: today) == Calendar.current.component(.minute, from: fini) &&
                //Calendar.current.component(.second, from: today) == Calendar.current.component(.second, from: fini) &&
                myColorScheme == .light {
                print("AFTERNOON! ------------------------")
                showingAlertAfternoon = true
            }
        }
    }
    
    

    
    let user: AuthUser
    var giftViewModel = GiftViewModel()
    var userViewModel = UserViewModel()
    var storeViewModel = StoreViewModel()
    
    var body: some View {
        TabView {
            FeedView(user: user)
                    .environmentObject(giftViewModel)
                    .tabItem {
                        Label("Feed", image: "home_icon")
                        Text("Feed")
                    }
            
            StoresView(user: user)
                    .environmentObject(giftViewModel)
                    .environmentObject(storeViewModel)
                    .tabItem {
                        Label("Stores", image: "store_icon")
                        Text("Stores")
                    }
             
            ExploreView(user: user).environmentObject(giftViewModel)
                .environmentObject(userViewModel)
                    .tabItem {
                        Label("Explore", image: "search_icon")
                        Text("Explore")
                    }
             
                ProfileView(user: user).environmentObject(userViewModel)
                    .tabItem {
                        Label("Profile", image: "profile_icon")
                        Text("Profile")
                    }
        }.accentColor(Color("GifterRed"))
            .onAppear{
                myColorScheme = storedColorScheme == "" ? systemColorScheme : storedColorScheme == "dark" ? .dark : .light
                RunLoop.main.add(self.timer, forMode: RunLoop.Mode.default)
            }
        VStack{}.alert(isPresented: $showingAlertMorning) {
            
                Alert(title: Text("It's early!"), message: Text("Do you want to turn on light mode ?"),  primaryButton: .default(Text("Change to light mode"), action: {
                    myColorScheme = .light
                    UserDefaults.standard.set("light", forKey: "ColorScheme")
                }),
                      secondaryButton: .cancel(Text("Continue in dark mode"), action: { // 1
                    UserDefaults.standard.set("dark", forKey: "ColorScheme")
                    
                }
            ))
            }.preferredColorScheme(myColorScheme ?? systemColorScheme)
        VStack{}.alert(isPresented: $showingAlertAfternoon) {
            
                Alert(title: Text("It's getting late!"), message: Text("Do you want to turn on dark mode ?"),  primaryButton: .default(Text("Change to dark mode"), action: {
                    myColorScheme = .dark
                    UserDefaults.standard.set("dark", forKey: "ColorScheme")
                }),
                      secondaryButton: .cancel(Text("Continue in light mode"), action: { // 1
                    UserDefaults.standard.set("light", forKey: "ColorScheme")
                }
            ))
            }.preferredColorScheme(myColorScheme ?? systemColorScheme)
    }
}



/*
struct GifterTabView_Previews: PreviewProvider {
    private struct DummyUser: AuthUser {
        let userId: String = "1"
        let username: String = "dummy"
        
    }
    static var previews: some View {
        GifterTabView(user: DummyUser())
    }
}
 */
