//
//  SignUpConfirmationView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 24/03/22.
//

import SwiftUI

struct SignUpConfirmationView: View {
    private enum Field: Int, CaseIterable {
            case username, confirmationCode
        }

    @Environment(\.colorScheme) var colorScheme
    @State var myColorScheme: ColorScheme?
    @State private var storedColorScheme = UserDefaults.standard.string(forKey: "ColorScheme")
    @EnvironmentObject var sessionManager: SessionManager
    
    @State var username: String = ""
    @State private var showingAlert = false
    let email: String
    @State private var confirmationCode: String = ""
    @FocusState private var focusedField: Field?
    
    var buttonColor: Color {
        return (confirmationCode.isEmpty || username.isEmpty) ? Color.ui.disabledGray : Color.ui.red
        }

    
    var textButtonColor: Color {
        return (confirmationCode.isEmpty || username.isEmpty) ? .black : .white
        
    }
    
    

    var body: some View {
        HStack(spacing: 0) {
            Spacer().frame(width: 16, height: 8, alignment: .trailing)
            VStack {
                GeometryReader { geometry in
                    VStack(alignment: .center){
                        HStack(spacing: 0) {
                            Button {
                                sessionManager.showInitialView()
                            } label: {
                                Image("chevron_left").foregroundColor(Color("GifterRed")).scaledToFit()
                                .transition(.slide)
                            }.buttonStyle(PlainButtonStyle())
                            Text("Confirm sign up").font(Font.custom("Montserrat-SemiBold",
                                                         size: 18,
                                                         relativeTo: .title
                                                            ))
                                .fontWeight(.semibold)
                                .foregroundColor(Color("GifterRed"))
                                .multilineTextAlignment(.center)
                                .frame(maxWidth: .infinity,  alignment: .center)
                                .scaledToFit()
                            

                        }
                        Spacer().frame(height: 104).scaledToFit()
                        HStack(alignment: .center){
                            Image("mail_icon").resizable()
                                .frame(width: 104, height: 80, alignment: .center)
                                .padding(.trailing, 8.0)
                        }
                        .frame(maxWidth: .infinity, alignment: .center)
                            
                            .padding(.leading, 20.0)
                    Spacer().frame(width: 32, height: 32)

                    (Text("We sent a confirmation code to \n ")
                     + Text("\(email).")
                                .foregroundColor(Color("GifterTurquoise"))
    
                     
                        
                         ).font(Font.custom("WorkSans-Regular",
                                                        size: 16
                                                          ))
                            .multilineTextAlignment(.center)
                        Spacer().frame(width: 24, height: 24)
          
                    TextField(
                                    "Username",
                                    text: $username
                                )
                                .focused($focusedField, equals: .username)
                                .padding()
                                .font(Font.custom("WorkSans-Regular",
                                                               size: 14
                                                                 ))
                                .frame(height: 40)

                                .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                .disableAutocorrection(true)
                    
                    Spacer().frame(width: 24, height: 24)

                    SecureField(
                                        "Confirmation code",
                                        text: $confirmationCode
                                    )
                                    .focused($focusedField, equals: .confirmationCode)
                                    .keyboardType(.numberPad)
                                    .padding()
                                    .font(Font.custom("WorkSans-Regular",
                                                                   size: 14
                                                                     ))
                                    .frame(height: 40)

                                    .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                    .disableAutocorrection(true)
                        GeometryReader { geometry_h in

                        VStack {
                            Spacer().frame(height: (geometry_h.size.height*0.24))
                            HStack{
                                Spacer().frame(width: geometry_h.size.width*0)
                                Button("Confirm my email", action: {
                                    //sessionManager.showCategories(username: username, confirmationCode: confirmationCode)
                                    if confirmationCode.isEmpty || username.isEmpty {
                                        showingAlert = true
                                    }
                                    sessionManager.confirm(username: username, code: confirmationCode)
                                    if sessionManager.errorMessage != "" {
                                        showingAlert = true
                                    }
                                })
                                .foregroundColor(textButtonColor)
                                .buttonStyle(StyledButton())
                                .background(RoundedRectangle(cornerRadius: 20, style:   .circular).fill(buttonColor))
                                .font(Font.custom("WorkSans-Regular",
                                                  size: 16,
                                                  relativeTo: .title
                                                  ))
                                .alert(isPresented: $showingAlert) {
                                
                                    Alert(title: Text("Sign up confirmation Error"), message: sessionManager.errorMessage != "" ? Text(sessionManager.errorMessage) : Text("The confirmation code is a required field") , dismissButton: Alert.Button.cancel(
                                        Text("OK"), action: {
                                            sessionManager.errorMessage = ""
                                        }
                                    ))
                                }
                                
                            
                            }.frame(maxWidth: .infinity, alignment: .center)
  
                        }
                        .buttonStyle(.bordered)
                        }
                    }
                    }
                
            }
            Spacer().frame(width: 24, height: 8)
            }.onTapGesture {
                let keyWindow = UIApplication.shared.connectedScenes
                                   .filter({$0.activationState == .foregroundActive})
                                   .map({$0 as? UIWindowScene})
                                   .compactMap({$0})
                                   .first?.windows
                                   .filter({$0.isKeyWindow}).first
                keyWindow!.endEditing(true)

            }
            .onAppear {
                myColorScheme = storedColorScheme == "" ? colorScheme : storedColorScheme == "dark" ? .dark : .light
            }
            .preferredColorScheme(myColorScheme ?? colorScheme)
    }
}



struct SignUpConfirmationView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpConfirmationView(email: "you@example.com")
    }
}
