//
//  ExploreView.swift
//  Gifter
// https://www.youtube.com/watch?v=KlS7h1OCgsY
//  Created by Sofía Álvarez on 25/03/22.
//

import SwiftUI
import Amplify

struct ExploreView: View {

    @Environment(\.colorScheme) var colorScheme
    @EnvironmentObject var sessionManager: SessionManager
    @EnvironmentObject var giftViewModel: GiftViewModel
    @EnvironmentObject var userViewModel: UserViewModel
    @State private var showingCategorySheet = false
    @State var isLoading = true
    
    @State var searchText: String = ""
    @Namespace var animation
    @State var categoryCards: [CategoryCard] = []
    let user: AuthUser
    
    // To show dynamic
    @State var columns: Int = 2
    
    var body: some View {
       /*NavigationView {
                        List {
                            ForEach(searchResults, id: \.self) { name in
                                NavigationLink(destination: Text(name)) {
                                    Text(name)
                                }
                            }
                        }                        .searchable(text: $searchText, prompt: "Search category...")

        }*/
            VStack {
                Button("Filters", action: {
                    showingCategorySheet.toggle()
                })
                    .sheet(isPresented: $showingCategorySheet) {
                        SelectCategorySheetView(user: user)
                        
                    }
                    .disabled(true) //For this sprint, this functionality will be disabled.
                    .frame(maxWidth: .infinity, alignment: .trailing)
                    .padding(.trailing, 16)
                    .font(Font.custom("WorkSans-Regular",size: 14))
                    .foregroundColor(Color.ui.disabledGray)
                Text("Your favorite categories")
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading, 16)
                    .font(Font.custom("Montserrat-SemiBold", size: 22))
                
                StaggeredGrid(columns: columns, list: categoryCards, content: { categoryCard in
                    CategoryCardView(categoryCard: categoryCard)
                        
                        
                }).redacted(reason: isLoading ? .placeholder : .init())
                .padding(.horizontal)
                
            }
        .onAppear {
            userViewModel.getUser(authUsername: user.username)
            
            isLoading = false
            for i in userViewModel.interests {
                let card = CategoryCard(id: i, title: i, imageName: i)
                if !self.categoryCards.contains(card) {
                    self.categoryCards.append(card)
                    }
                }
            }
        .onDisappear {
            userViewModel.interests = []
        }
        }
    }


/*let categoryCards: [CategoryCard] = [
    .init(id: 1,
          title: "Women",
          imageName: "women"),
    .init(id: 2,
          title: "Movies, music & books",
          imageName: "movmusboo"),
    .init(id: 3,
          title: "Home",
          imageName: "home"),
    .init(id: 4,
          title: "Kitchen & Dining",
          imageName: "kitchendining"),
    .init(id: 5,
        title: "Musical instruments",
          imageName: "instruments"),
    .init(id: 6,
        title: "Garden",
        imageName: "garden"),
    .init(id: 7,
        title: "School office supplies",
        imageName: "school"),
    .init(id: 8,
        title: "Toys",
        imageName: "toys")
]*/


struct CategoryCard: Identifiable, Hashable {
    var id: String
    let title: String
    let imageName: String
}

struct CategoryCardView: View {
    @Environment(\.colorScheme) var colorScheme
    var contentMode: ContentMode = .fit
    var categoryCard: CategoryCard
    var body: some View {
        Button {
            // Just for this sprint, this functionality will be temporarily disabled.
        } label: {
            Image(categoryCard.imageName).resizable().cornerRadius(10).background(RoundedRectangle(cornerRadius: 10).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white))
            
                .aspectRatio(contentMode: contentMode)
        }.buttonStyle(PlainButtonStyle()).transition(.slide)
          //  .disabled(true) // Just for this sprint, this functionality will be temporarily disabled.
    }
}

struct SelectCategorySheetView: View {
    let user: AuthUser
    @EnvironmentObject var userViewModel: UserViewModel
    @Environment(\.dismiss) var dismiss
    @State var categories = ["Movies, music & books", "Home", "Kitchen & Dining", "Musical instruments", "School office supplies",
    "Garden", "Women", "Toys"]
    @State var selections: [String] = []
    
    var body: some View {
        Button("Done") {
           // userViewModel.updateInterests(authUsername: user.username, interests: [])
            userViewModel.updateInterests(authUsername: user.username, interests: userViewModel.interests)
            dismiss()
        }
        .frame(maxWidth: .infinity, alignment: .trailing).padding(.trailing, 16).padding(.top, 8)
        List {
             ForEach(self.categories, id: \.self) { item in
                 MultipleSelectionRow(title: item, isSelected: self.selections.contains(item)) {
                     if self.selections.contains(item) {
                         self.selections.removeAll(where: { $0 == item })
                         userViewModel.interests.removeAll(where: { $0 == item })
                     }
                     else {
                         self.selections.append(item)
                         userViewModel.interests.append(item)
                     }
                 }
             }
        }.onAppear{
            selections = userViewModel.interests
        }
    }
 }

struct MultipleSelectionRow: View {
    var title: String
    var isSelected: Bool
    var action: () -> Void

    var body: some View {
        Button(action: self.action) {
            HStack {
                Text(self.title)
                if self.isSelected {
                    Spacer()
                    Image(systemName: "checkmark")
                }
            }
        }
    }
}
