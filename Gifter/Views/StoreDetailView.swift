//
//  StoreDetailView.swift
//  Gifter
//
//  Created by Juliana Prieto Arcila on 25/03/22.
//

import SwiftUI
import MapKit
import CoreLocation
import WaterfallGrid

struct StoreDetailView: View {
    @EnvironmentObject var storeViewModel: StoreViewModel
    @EnvironmentObject var giftViewModel: GiftViewModel
    @Environment(\.colorScheme) var colorScheme
    
    let store: StoreDTO
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                AsyncImage(url: URL(string: store.images)){
                    phase in
                    switch phase {
                    case .empty:
                        withAnimation(.easeInOut(duration: 1)) {
                        Rectangle().fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.ui.gifterGray)
                            .frame(maxWidth: .infinity)
                            .frame(height: 200)
                        }
                    case .success(let image):
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .scaledToFill()
                            .frame(maxWidth: .infinity, maxHeight: 200)
                            .clipped()
                            .background(RoundedRectangle(cornerRadius: 10).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white))
                            .overlay(
                                LinearGradient(
                                    colors: [Color(red: 255, green: 255, blue: 255, opacity: 0.4), Color(red: 255, green: 255, blue: 255, opacity: 0)],
                                    startPoint: .top,
                                    endPoint: .bottom
                                )
                            )
                            
                    case .failure:
                        ZStack {}
                     @unknown default:
                        // Since the AsyncImagePhase enum isn't frozen,
                        // we need to add this currently unused fallback
                        // to handle any new cases that might be added
                        // in the future:
                        ZStack {}
                    }
                }
                
                Text(store.name)
                    .font(Font.custom("Montserrat-SemiBold", size: 24))
                    .foregroundColor(Color("GifterTextDefault"))
                    .padding([.leading, .top], 16)
                    .padding(.bottom, 0.5)
                
                Text(store.description)
                    .font(Font.custom("WorkSans-Regular", size: 16))
                    .foregroundColor(Color("GifterTextDefault"))
                    .padding(.leading, 16)
                    .padding(.bottom, 16)
                
                ScrollView(.horizontal) {
                    HStack {
                        WaterfallGrid(storeViewModel.storeGifts[store.id] ?? [], id: \.self) { gift in
                            GiftView(gift: gift)
                        }
                        .gridStyle(columns: 1, animation: .easeInOut(duration: 0.5))
                        .padding([.leading, .trailing], 16)
                        .scrollOptions(direction: .horizontal)
                        .frame(height: 200)
                    }
                    .padding(.leading, 16)
                }
                
                Color("GifterBackground")
                    .frame(height: 16)
                    .padding(.top, 16)
                
                Text("Location")
                    .font(Font.custom("Montserrat-SemiBold", size: 20))
                    .foregroundColor(Color("GifterTextDefault"))
                    .padding([.leading, .bottom], 16.0)
                    .padding(.top, 4)
                
                StoreMapView(store: store)
                
                Spacer()
            }
            .navigationBarTitle(store.name)
            .navigationBarTitleDisplayMode(.inline)
            .onAppear{
//                storeViewModel.getStoreGifts(storeId: store.id)
//                storeViewModel.getEachStoreGifts(store: store)
            }
        }
    }
}

struct StoreDetailView_Previews: PreviewProvider {
    static var previews: some View {
        StoreDetailView(store: StoreDTO(
            distance:"0.44",
            id: 3,
            location: Location(_latitude: -74.11037944, _longitude: 4.666060168),
            address: "Cra. 68B # 25B - 80",
            images: "https://arkadiacentrocomercial.com/wp-content/uploads/2021/09/Ktronix.jpg",
            description: "First store specialized in technology. Find offers on appliances and technology.",
            name: "Ktronix",
            website: "ktronix.com",
            gifts: [
                "0666855e-e1a2-446d-848e-864a92774721",
                "2a49853d-f818-4a46-8008-6ee9ef256809",
                "8c5e7127-d19a-40bf-92dd-3073e107bf67",
                "a56308af-abd2-41a0-9cf3-b4a040fd8d3f",
                "7f7e0965-52f0-4152-8d3b-89d11da45739",
                "a17b4428-be23-4ccf-820c-e4d502b18979",
                "e6a872ab-6a79-4703-9148-1efdca2289e5",
                "b1611948-8438-4332-bfed-8742c2780620",
                "2f71e413-32fe-4f1d-a28b-8995be0685e8",
                "3f3b3c2d-b2ca-450f-9280-98f29b3371d5",
                "b9e6a202-3fff-460d-997f-e3dc9e7d83f3"
            ],
            phone: "4073033"
        ))
    }
}

struct StoreMapView: View {
    @StateObject private var storeMapViewModel = StoreMapViewModel()
    @EnvironmentObject var giftViewModel: GiftViewModel
    @State private var userTrackingMode: MapUserTrackingMode = .follow
    let store: StoreDTO
//    let maplocation = MapLocation(name: "hola", coordinate: CLLocationCoordinate2D(latitude: -74.11037944, longitude: 4.666060168))
//    let storeLocation = CLLocationCoordinate2D(latitude: store.location._latitude, longitude: store.location._longitude)
    
    var body: some View {
      Map(
        coordinateRegion: $storeMapViewModel.coordinateRegion,
        showsUserLocation: true,
        userTrackingMode: $userTrackingMode,
        annotationItems: [store],
        annotationContent: { store in
            MapMarker(coordinate: CLLocationCoordinate2D(latitude: store.location._longitude, longitude: store.location._latitude), tint: Color("GifterRed"))
        }
      )
        .padding([.leading, .trailing], 16)
        .frame(height: 500)
        .accentColor(Color("GifterTurquoise"))
        .onAppear {
            storeMapViewModel.checkIfLocationServicesIsEnabled()
        }
    }
}

struct MapLocation: Identifiable {
    let id = UUID()
    let name: String
    let coordinate: CLLocationCoordinate2D
}
