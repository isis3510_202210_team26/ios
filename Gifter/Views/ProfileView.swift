//
//  ProfileView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 25/03/22.
//

import Amplify
import SwiftUI
import WaterfallGrid
import SwiftUIPullToRefresh


struct ProfileView: View {
    let user: AuthUser
    @EnvironmentObject var sessionManager: SessionManager
    @EnvironmentObject var viewModel: UserViewModel
    @State var intValue: Int = 0
    @State private var profilePic: Image? = Image("defaultProfile")
    @State private var coverImg: Image? = Image("defaultCoverImg")
    @State private var shouldPresentImagePicker = false
    @State private var shouldPresentActionSheet = false
    @State private var shouldPresentCamera = false
    @State private var shouldPresentImagePickerCover = false
    @State private var shouldPresentActionSheetCover = false
    @State private var shouldPresentCameraCover = false
    @State private var showDescription = false
    @State private var refresh = false
    
    let radius: CGFloat = 100
    var offset: CGFloat {
        sqrt(radius * radius / 2)
    }
    
    func downloadPicture(key: String, profile: Bool) {
        _ = Amplify.Storage.downloadData(key: key) {result in
            switch result{
            case .success(let imageData):
                if profile {
                    self.profilePic = Image(uiImage: UIImage(data: imageData)!)
                }
                else {
                    self.coverImg = Image(uiImage: UIImage(data: imageData)!)
                }
                
            
            case .failure(let error):
                print(error)
            }
            
        }
    }

    func didTapButtonProfilePic() {
        if let image = self.profilePic {
            viewModel.uploadProfilePic(authUsername: user.username, image)
        }
    }
    
    func didTapButtonCoverImg() {
        if let image = self.coverImg {
            viewModel.uploadCoverImg(authUsername: user.username, image)
        }
    }
    

    
    var body: some View {
        NavigationView {
        //ScrollView {
        RefreshableScrollView(onRefresh: { done in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                      //self.now = Date()
                        viewModel.getUser(authUsername: user.username)
                      done()
                    }
                  }) {
        VStack{
            coverImg!.resizable(capInsets: EdgeInsets()).edgesIgnoringSafeArea(.all).offset(y: -50)
                .frame(
                    height: 260)
              
                .overlay(
                Button(action: {}) {
                    
                    Image("camera")
                        .foregroundColor(.primary)
                        .padding(8)
                        .background(Color("GifterGray"))
                        .clipShape(Circle())
                        .background(
                            Circle()
                                .stroke(Color.white, lineWidth: 2)
                        )
                        .onTapGesture {
                            self.shouldPresentActionSheetCover = true
                        }
                        .sheet(isPresented: $shouldPresentImagePickerCover, onDismiss: didTapButtonCoverImg
                        ) {
                                        // Pick an image from the photo library:
                            ImagePicker(sourceType: self.shouldPresentCameraCover ? .camera : .photoLibrary, image: self.$coverImg, isPresented: self.$shouldPresentImagePickerCover)
                       
                        
                        }
                        .actionSheet(isPresented: $shouldPresentActionSheetCover) { () -> ActionSheet in
                                    ActionSheet(title: Text("Choose mode"), message: Text("Please choose your preferred mode to set your cover image"), buttons: [ActionSheet.Button.default(Text("Camera"), action: {
                                        self.shouldPresentImagePickerCover = true
                                        self.shouldPresentCameraCover = true
                                    }), ActionSheet.Button.default(Text("Photo Library"), action: {
                                        self.shouldPresentImagePickerCover = true
                                        self.shouldPresentCameraCover = false
                                    }), ActionSheet.Button.cancel()])
                                }
                        
                    
                    
                }.offset(x: 2*offset + offset/3, y: offset - offset/2)
                   
            )
        profilePic!.resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(
                            width: radius * 2,
                            height: radius * 2)
                        .clipShape(Circle())
                        .offset(y: -200)
                        .padding(.bottom, -380)
                        .overlay(
                            Button(action: didTapButtonProfilePic) {
                                
                                Image("camera")
                                    .foregroundColor(.primary)
                                    .padding(8)
                                    .background(Color("GifterGray"))
                                    .clipShape(Circle())
                                    .background(
                                        Circle()
                                            .stroke(Color.white, lineWidth: 2)
                                    )
                                    .onTapGesture {
                                        self.shouldPresentActionSheet = true
                                    }
                                    .sheet(isPresented: $shouldPresentImagePicker, onDismiss: didTapButtonProfilePic) {
                                                    // Pick an image from the photo library:
                                        ImagePicker(sourceType: self.shouldPresentCamera ? .camera : .photoLibrary, image: self.$profilePic, isPresented: self.$shouldPresentImagePicker)
                                   
                                    
                                    }
                                    .actionSheet(isPresented: $shouldPresentActionSheet) { () -> ActionSheet in
                                                ActionSheet(title: Text("Choose mode"), message: Text("Please choose your preferred mode to set your profile picture"), buttons: [ActionSheet.Button.default(Text("Camera"), action: {
                                                    self.shouldPresentImagePicker = true
                                                    self.shouldPresentCamera = true
                                                }), ActionSheet.Button.default(Text("Photo Library"), action: {
                                                    self.shouldPresentImagePicker = true
                                                    self.shouldPresentCamera = false
                                                }), ActionSheet.Button.cancel()])
                                            }
                                    
                                
                                
                            }.offset(x: offset, y: -42.5)
                        )
                    
            Text("\(user.username)").frame(maxWidth: .infinity,  alignment: .center).font(Font.custom("WorkSans-Regular", size: 24))
            if showDescription {
                Spacer().frame(width: 16, height: 16)
                Text("\(viewModel.bio)").padding(.horizontal).font(Font.custom("WorkSans-Regular", size: 14))            }
            else {
                Spacer().frame(width: 8, height: 8)
            }
            Spacer().frame(width: 16, height: 16)
            HStack{
                NavigationLink(destination: EditProfileView(user: user).environmentObject(viewModel)) {
                    Button("Edit Profile", action: {})
                    .foregroundColor(.white)
                    .buttonStyle(ProfileStyledButton())
                    .background(RoundedRectangle(cornerRadius: 20, style:   .circular).fill(Color.ui.red))
                    .font(Font.custom("WorkSans-Regular",
                                      size: 12,
                                      relativeTo: .title
                                      ))
                    .disabled(true)
                }
                Button("Log Out", action: {sessionManager.signOut()})
                    .foregroundColor(.white)
                    .buttonStyle(ProfileStyledButton())
                    .background(RoundedRectangle(cornerRadius: 20, style:   .circular).fill(Color.ui.red))
                    .font(Font.custom("WorkSans-Regular",
                                      size: 12,
                                      relativeTo: .title
                                      ))
            }.frame(maxWidth: .infinity, alignment: .center)
            
            Spacer().frame(width: 24, height: 24)
            Text("Your favorite gift ideas").padding(.leading).font(Font.custom("Montserrat-SemiBold", size: 16)).frame(maxWidth: .infinity,  alignment: .leading)
            WaterfallGiftGridProfile().padding(.top, 16)
        }
      }
        }.onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                viewModel.getUser(authUsername: user.username)
                if viewModel.coverImg != "" {
                    downloadPicture(key: viewModel.coverImg, profile: false)
                 }
                if viewModel.profilePic != "" {
                    downloadPicture(key: viewModel.profilePic, profile: true)
                 }
                if viewModel.bio != "" {
                    showDescription = true
                }
            }
           
            
            
        }
        .padding(.top, -152)
  }
}

struct ProfileView_Previews: PreviewProvider {
    
    private struct DummyUser: AuthUser {
        let userId: String = "1"
        let username: String = "dummy"
    }
    static var previews: some View {
        ProfileView(user: DummyUser())
    }
}

struct ProfileStyledButton: ButtonStyle {
   func makeBody(configuration: Configuration) -> some View {
       configuration
           .label
           .padding()
           .frame(width: 104, height: 32)
   }
}

struct WaterfallGiftGridProfile: View {
    var scrollingDirection: Axis.Set = .vertical
    var columns: Int = 2
    
    var body: some View {
        WaterfallGrid((5..<10), id: \.self) { index in
            GiftViewProfile(index: index)
        }
        .gridStyle(columns: columns, animation: .easeInOut(duration: 0.5))
        .padding([.leading, .trailing], 16)
       .scrollOptions(direction: scrollingDirection)
    }
}

struct GiftViewProfile: View {
    
    let index: Int
    var contentMode: ContentMode = .fit
    
    let alignment = Alignment(horizontal: .trailing, vertical: .top)
    
    @State private var addedToWishlist = true
    
    var body: some View {
        ZStack(alignment: alignment) {
            Image("Gift\(index)")
                .resizable()
                .aspectRatio(contentMode: contentMode)
                .overlay(
                    LinearGradient(
                        colors: [Color(red: 0, green: 0, blue: 0, opacity: 0.3), Color(red: 0, green: 0, blue: 0, opacity: 0)],
                        startPoint: .topTrailing,
                        endPoint: .bottomLeading
                    )
                )
                .cornerRadius(10)
                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.1), radius: 5, x: 0, y: 2)
            
            addToWishlistButtonView(addedToWishlist: $addedToWishlist)
        }
        
    }
}


extension View {
// This function changes our View to UIView, then calls another function
// to convert the newly-made UIView to a UIImage.
    public func asUIImage() -> UIImage {
        let controller = UIHostingController(rootView: self)
        
        controller.view.frame = CGRect(x: 0, y: CGFloat(Int.max), width: 1, height: 1)
        UIApplication.shared.windows.first!.rootViewController?.view.addSubview(controller.view)
        
        let size = controller.sizeThatFits(in: UIScreen.main.bounds.size)
        controller.view.bounds = CGRect(origin: .zero, size: size)
        controller.view.sizeToFit()
        
// here is the call to the function that converts UIView to UIImage: `.asUIImage()`
        let image = controller.view.asUIImage()
        controller.view.removeFromSuperview()
        return image
    }
}

extension UIView {
// This is the function to convert UIView to UIImage
    public func asUIImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
