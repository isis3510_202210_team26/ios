//
//  SignUpView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 24/03/22.
//
import Amplify
import SwiftUI
import Combine
import AWSPluginsCore



struct SignUpView: View {
    
    @Environment(\.colorScheme) var colorScheme
    @State var myColorScheme: ColorScheme?
    @State private var storedColorScheme = UserDefaults.standard.string(forKey: "ColorScheme")
    @EnvironmentObject var sessionManager: SessionManager
    @StateObject var viewModel = UserViewModel()
    @State private var password: String = ""
    @State private var confirmEmail: String = ""
    @State private var showingAlert = false
    @State private var disabled = true

    
    
    private func isValidEmail(_ email: String) -> Bool {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailPred.evaluate(with: email)
        }
    
    private func validate(password: String) -> Bool {
        let capitalLetterRegEx  = ".*[A-Za-z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        guard texttest.evaluate(with: password) else { return false }

        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        guard texttest1.evaluate(with: password) else { return false }

        return true
    }
    
    private func calculateAge(birthdate: Date) -> Int {
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthdate, to: Date())
        return ageComponents.year!
    }
    
    private func isReady(name: String, email: String, username: String, password: String, birthdate: Date) -> Bool {
        return !viewModel.name.isEmpty && !viewModel.email.isEmpty && isValidEmail(viewModel.email) && viewModel.email == confirmEmail
        && !viewModel.username.isEmpty && validate(password: password) && !password.isEmpty && password.count >= 8 && (calculateAge(birthdate: birthdate) >= 5)
    }



    var body: some View {
        ScrollView{
            HStack(spacing: 0) {
                Spacer().frame(width: 16, height: 8, alignment: .trailing)
                VStack {
                        HStack(spacing: 0) {
                            //
                            Button {
                                sessionManager.showInitialView()
                            } label: {
                                Image("chevron_left").foregroundColor(Color("GifterRed"))
                            }.buttonStyle(PlainButtonStyle()).transition(.slide)
                            Text("Sign up").font(Font.custom("Montserrat-SemiBold",
                                                         size: 18,
                                                         relativeTo: .title
                                                            ))
                                .fontWeight(.semibold)
                                .foregroundColor(Color("GifterRed"))
                                .multilineTextAlignment(.center)
                                .frame(maxWidth: .infinity,  alignment: .center)
                                .padding(.leading, -32)
                            

        
                    }
                        VStack(alignment: .leading) {
                            VStack(alignment: .leading){
                                Spacer().frame(height: 40).scaledToFit()
                                (Text("What's your ")
                                        + Text("name")
                                        .foregroundColor(Color("GifterTurquoise"))
                                 + Text("?")).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                                  ))
                                Spacer().frame(height: 16).scaledToFit()
                                TextField(
                                            "Name and last name",
                                            text: $viewModel.name
                                        )
                                .onReceive(Just(viewModel.name)) { inputValue in
                                            if inputValue.count > 30 {
                                                viewModel.name.removeLast()
                                            }
                                        }
                                
                                        .padding()
                                        .font(Font.custom("WorkSans-Regular",
                                                                       size: 14
                                                                         ))
                                        .frame(height: 40)
                                        .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                        .disableAutocorrection(true)
                                HStack {
                                        Spacer().frame(width: 16, height: 4)
                                    Text(viewModel.name.isEmpty ? "The name field is required" : "").foregroundColor(Color("GifterError"))
                                             .font(Font.custom("WorkSans-Regular",
                                                               size: 12
                                                               ))
                               }
                            }
                            VStack(alignment: .leading){
                                Spacer().frame(height: 16).scaledToFit()
                                (Text("What's your ")
                                        + Text("email")
                                        .foregroundColor(Color("GifterTurquoise"))
                                 + Text("?")).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                                  ))
                                            
                                Spacer().frame(height: 16).scaledToFit()
                                TextField(
                                            "you@email.com",
                                            text: $viewModel.email
                                        )
                                        .onReceive(Just(viewModel.email)) { inputValue in
                                            if inputValue.count > 40 {
                                                viewModel.email.removeLast()
                                            }
                                        }
                                        .keyboardType(.emailAddress)

                                        .padding()
                                        .font(Font.custom("WorkSans-Regular",
                                                                       size: 14
                                                                         ))
                                        .frame(height: 40)
   
                                        .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                        .disableAutocorrection(true)
                                HStack {
                                        Spacer().frame(width: 16, height: 4).scaledToFit()
                                    Text(viewModel.email.isEmpty ? "The email field is required" : !isValidEmail(viewModel.email) ? "Invalid email format" : "" ).foregroundColor(Color("GifterError"))
                                             .font(Font.custom("WorkSans-Regular",
                                                               size: 12
                                                               ))
                               }
                            }
                            VStack(alignment: .leading){
                                Spacer().frame(height: 16).scaledToFit()
                                (Text("Confirm your ")
                                        + Text("email")
                                        .foregroundColor(Color("GifterTurquoise"))).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                                  ))
                                            
                                Spacer().frame(height: 16).scaledToFit()
                                TextField(
                                            "you@email.com",
                                            text: $confirmEmail
                                        )
                                        .onReceive(Just(confirmEmail)) { inputValue in
                                            if inputValue.count > 40 {
                                                confirmEmail.removeLast()
                                            }
                                        }
                                        .keyboardType(.emailAddress)

                                        .padding()
                                        .font(Font.custom("WorkSans-Regular",
                                                                       size: 14
                                                                         ))
                                        .frame(height: 40)
   
                                        .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                        .disableAutocorrection(true)
                                HStack {
                                        Spacer().frame(width: 16, height: 4).scaledToFit()
                                    Text(confirmEmail.isEmpty ? "The email field is required" : !isValidEmail(confirmEmail) ? "Invalid email format" : viewModel.email == confirmEmail ? "" : "Confirmation email must match" ).foregroundColor(Color("GifterError"))
                                             .font(Font.custom("WorkSans-Regular",
                                                               size: 12
                                                               ))
                               }
                            }
                            VStack(alignment: .leading){
                                Spacer().frame(height: 16).scaledToFit()
                                (Text("Think of a cool ")
                                        + Text("username")
                                        .foregroundColor(Color("GifterTurquoise"))
                                 ).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                                  ))
                                Spacer().frame(height: 16).scaledToFit()
                                TextField(
                                            "coolusername",
                                            text: $viewModel.username
                                        )
                                        .onReceive(Just(viewModel.username)) { inputValue in
                                            if inputValue.count > 20 || inputValue.contains(" ") {
                                                viewModel.username.removeLast()
                                            }
                                            
                                        }
                                        .padding()
                                        .font(Font.custom("WorkSans-Regular",
                                                                       size: 14
                                                                         ))
                                        .frame(height: 40)
   
                                        .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                        .disableAutocorrection(true)
                                HStack {
                                        Spacer().frame(width: 16, height: 4)
                                         Text(viewModel.username.isEmpty ? "The username field is required" : "").foregroundColor(Color("GifterError"))
                                             .font(Font.custom("WorkSans-Regular",
                                                               size: 12
                                                               ))
                               }
                            }
                            VStack(alignment: .leading){
                                Spacer().frame(height: 16)
                                (Text("Set your ")
                                        + Text("password")
                                        .foregroundColor(Color("GifterTurquoise"))
                                 ).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                                  ))
                                Spacer().frame(height: 16).scaledToFit()
                                SecureField(
                                            "********",
                                            text: $password
                                        )
                                            .onReceive(Just(password)) { inputValue in
                                            if inputValue.count > 20 {
                                                password.removeLast()
                                            }
                                        }
                                        .padding()
                                        .font(Font.custom("WorkSans-Regular",
                                                                       size: 14
                                                                         ))
                                        .frame(height: 40)
   
                                        .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                        .disableAutocorrection(true)
                                
                                VStack(alignment: .leading){
                                    HStack{
                                        Spacer().frame(width: 16, height: 4)
                                        Text("The password must be alphanumeric").foregroundColor(validate(password: password) && !password.isEmpty ? Color("GifterSuccess") : Color("GifterError"))
                                             .font(Font.custom("WorkSans-Regular",
                                                               size: 12
                                                              ))
                                    }
                                    HStack{
                                        Spacer().frame(width: 16, height: 4)
                                        Text("The password must have at least 8 characters").foregroundColor(password.count >= 8 ? Color("GifterSuccess") : Color("GifterError"))
                                                 .font(Font.custom("WorkSans-Regular",
                                                                   size: 12
                                                                   ))
                                    }
                               }
                                   
                               
                            }
                            VStack(alignment: .leading){
                                Spacer().frame(height: 16)
                                (Text("What's your ")
                                        + Text("birthdate")
                                        .foregroundColor(Color("GifterTurquoise"))
                                 + Text("?")).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                              ))
                                Spacer().frame(height: 16)
                                DatePicker(selection: $viewModel.birthdate, in: ...Date(), displayedComponents: .date) {
                                                Text("Select your birthdate").font(Font.custom("WorkSans-Regular",
                                                                               size: 14
                                                                                 ))
                                                    
                                            }//.datePickerStyle(.compact)
                                            .accentColor(Color.ui.red)
                                HStack {
                                        Spacer().frame(width: 16, height: 4)
                                    Text(calculateAge(birthdate: viewModel.birthdate) < 5 ? "You must be at least 5 years old" : "").foregroundColor(Color("GifterError"))
                                             .font(Font.custom("WorkSans-Regular",
                                                               size: 12
                                                               ))
                               }
                                
                                            
                            }
                 
                            GeometryReader { geometry_h in

                            VStack {
                                Spacer().frame(height: (geometry_h.size.height*0.24))
                                HStack{
                                    Spacer().frame(width: geometry_h.size.width*0)
                                Button("Sign up", action: {
                                    if isReady(name: viewModel.name, email: viewModel.email, username: viewModel.username, password: password, birthdate: viewModel.birthdate) {
                                        self.disabled = false
                                        sessionManager.signUp(
                                        username: viewModel.username,
                                        email: viewModel.email,
                                        password: password)
                                        viewModel.createUser(authUsername: viewModel.username)
                                    }
                                    else {
                                        showingAlert = true
                                    }
                                    if sessionManager.errorMessage != "" || viewModel.error != "" {
                                        showingAlert = true
                                    }
                                   // viewModel.createUser(authUsername: viewModel.username)
                                })
                                .foregroundColor(isReady(name: viewModel.name, email: viewModel.email, username: viewModel.username, password: password, birthdate: viewModel.birthdate) ? .white : .black)
                                    .buttonStyle(StyledButton())
                                    .background(RoundedRectangle(cornerRadius: 20, style:   .circular).fill(isReady(name: viewModel.name, email: viewModel.email, username: viewModel.username, password: password, birthdate: viewModel.birthdate) ? Color.ui.red : Color.ui.disabledGray))
                                    .font(Font.custom("WorkSans-Regular",
                                                      size: 16,
                                                      relativeTo: .title
                                                      ))
                                    .alert(isPresented: $showingAlert) {
                                    
                                        Alert(title: Text("Sign up Error"), message: sessionManager.errorMessage != "" ? Text(sessionManager.errorMessage) : viewModel.error != "" ? Text(viewModel.error) : Text("All fields must be complete and correct") , dismissButton: Alert.Button.cancel(
                                            Text("OK"), action: {
                                                viewModel.error = ""
                                                sessionManager.errorMessage = ""
                                            }
                                        ))
                                    }
                                   
                                    
                                }.frame(maxWidth: .infinity, alignment: .center)
                            }
                            .buttonStyle(.bordered)
                                
                            }
                        }.padding(.leading, -8)
                        .padding(.trailing, 8)
                        .padding(.top, -28)
                    }
                    .ignoresSafeArea(.keyboard)
                }
        }

        .onTapGesture {
            let keyWindow = UIApplication.shared.connectedScenes
                               .filter({$0.activationState == .foregroundActive})
                               .map({$0 as? UIWindowScene})
                               .compactMap({$0})
                               .first?.windows
                               .filter({$0.isKeyWindow}).first
            keyWindow!.endEditing(true)
        }
        .onAppear {
            myColorScheme = storedColorScheme == "" ? colorScheme : storedColorScheme == "dark" ? .dark : .light
        }
        .preferredColorScheme(myColorScheme ?? colorScheme)
    }
}


