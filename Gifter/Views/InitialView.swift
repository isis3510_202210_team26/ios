//
//  InitialView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 24/03/22.
//

import SwiftUI

struct InitialView: View {
    @Environment(\.colorScheme) var colorScheme
    @State var myColorScheme: ColorScheme?
    @State private var storedColorScheme = UserDefaults.standard.string(forKey: "ColorScheme")
    @EnvironmentObject var sessionManager: SessionManager
    @State var showSignUpView: Bool = false
    @ScaledMetric(relativeTo: .body) var scaledPadding: CGFloat = 8
    var body: some View {
        VStack{
            VStack  {
                Image("app_header_larger")
                    .resizable(capInsets: EdgeInsets())
                
            }.edgesIgnoringSafeArea(.all)
            Spacer().frame(width: 0, height: 54, alignment: .center)
                .scaledToFit()
            Image("icon")
                .scaledToFit()
            Spacer().frame(width: 0, height: 36, alignment: .center)
                .scaledToFit()
            Text("Gifter")
                .font(Font.custom("PTSerif-Bold",
                                  size: 42,
                                  relativeTo: .title
                                  ))
                .scaledToFit()
            Spacer().frame(width: 0, height: 24, alignment: .center)
                .scaledToFit()
            Text("Find the perfect gift... always")
                .font(Font.custom("WorkSans-Regular",
                                  size: 16,
                                  relativeTo: .title
                                  ))
                .scaledToFit()
            Spacer().frame(width: 0, height: 40, alignment: .center)
                .scaledToFit()
            VStack {
                Button("Sign up", action: {sessionManager.showSignUp()})
                    .foregroundColor(.white)
                    .buttonStyle(StyledButton())
                    .background(RoundedRectangle(cornerRadius: 20, style:   .circular).fill(Color.ui.red))
                    .font(Font.custom("WorkSans-Regular",
                                      size: 16,
                                      relativeTo: .title
                                      ))
            
                Spacer().frame(width: 0, height: 8, alignment: .center)
                    .scaledToFit()
                Button("Log in", action: {sessionManager.showLogIn()})
                    .buttonStyle(StyledButton())
                    .font(Font.custom("WorkSans-Regular",
                                      size: 16,
                                      relativeTo: .title
                                      ))
                
            }
            .buttonStyle(.bordered)
            Spacer().frame(width: 0, height: 49, alignment: .center)
                .scaledToFit()

        }.onAppear {
            myColorScheme = storedColorScheme == "" ? colorScheme : storedColorScheme == "dark" ? .dark : .light
        }
        .preferredColorScheme(myColorScheme ?? colorScheme)
    }
}


struct InitialView_Previews: PreviewProvider {
    static var previews: some View {
        InitialView()
    }
}
