////
////  NewFeed.swift
////  Gifter
////
////  Created by Sofía Álvarez on 21/04/22.
////
//
//import SwiftUI
//
////
////  File.swift
////  Gifter
////
////  Created by Juliana Prieto Arcila on 24/03/22.
////
//
//import SwiftUI
//import WaterfallGrid
//import MapKit
//import Amplify
//import SwiftUIPullToRefresh
//
//struct StoreInfo: Identifiable {
//    let id: Int
//    let name: String
//    let image: String
//    let description: String
//    let contactInfo: String
//    let location: CLLocationCoordinate2D
//}
//
//struct OldFeedView: View {
//    @Environment(\.colorScheme) var colorScheme
//    @EnvironmentObject var giftViewModel: GiftViewModel
//    @EnvironmentObject var sessionManager: SessionManager
//    let user: AuthUser
//    let alignment = Alignment(horizontal: .leading, vertical: .top)
//    let fontDefaultColor = Color("GifterTextDefault")
//
//    var gridItemLayout = [GridItem(.flexible()), GridItem(.flexible())]
//
//    var body: some View {
//        NavigationView {
//            RefreshableScrollView(onRefresh: { done in
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                         //   giftViewModel.getGifts()
//
//                          done()
//                        }
//                      }) {
//                VStack(alignment: .leading) {
//                    HStack(alignment: .center) {
//                        Image("icon")
//                            .resizable()
//                            .aspectRatio(contentMode: .fit)
//                            .frame(width: 30.0)
//                        Text("Gifter")
//                            .font(Font.custom("PTSerif-Bold", size: 24))
//                            .foregroundColor(colorScheme == .dark ? Color.white : fontDefaultColor)
//                            .padding(.top, 5.0)
//                    }
//                    .padding(.leading, 16.0)
//                    Text("Take a look at gift shops near you")
//                        .font(Font.custom("Montserrat-SemiBold", size: 18))
//                        .foregroundColor(colorScheme == .dark ? Color.white : fontDefaultColor)
//                        .padding([.leading, .bottom], 16.0)
//                        .padding(.top, 4)
//
//                    ScrollView(.horizontal) {
//                        HStack() {
//                            StoreCard(imageName: "icon", storeName: "Store 1", storeDescription: "Surprise breakfasts and more")
//
//                            StoreCard(imageName: "icon", storeName: "Store 2", storeDescription: "Gifts of any kind")
//                        }
//                        .padding(.top, 16.0)
//                    }
//                    .frame(height: 180)
//                    .padding(.bottom, 24)
//
//                    Color(colorScheme == .dark ? "black" : "GifterBackground")
//                        .frame(height: 16)
//
//                    Text("Popular gift ideas")
//                        .font(Font.custom("Montserrat-SemiBold", size: 22))
//                        .foregroundColor(colorScheme == .dark ? Color.white : fontDefaultColor)
//                        .padding(.leading, 16.0)
//                        .padding(.top, 10.0)
//                        .padding(.bottom, 16)
//
//                    WaterfallGiftGrid().environmentObject(giftViewModel).padding(.bottom, 88)
//
//                    Spacer()
//                }
//
//                      }
//            .navigationBarHidden(false)
//            .navigationBarTitleDisplayMode(.inline)
//            .navigationBarTitle(Text("Feed"))
//            .edgesIgnoringSafeArea(.bottom)
//            .onAppear {
//                giftViewModel.getGifts()
//            }
//        }.padding(.top, -96)
//    }
//
//}
//
//struct FeedView_Previews: PreviewProvider {
//    private struct DummyUser: AuthUser {
//        let userId: String = "1"
//        let username: String = "dummy"
//
//    }
//    static var previews: some View {
//        OldFeedView(user: DummyUser()).environmentObject(GiftViewModel())
//    }
//}
//
//struct StoreCard: View {
//    @Environment(\.colorScheme) var colorScheme
//    let imageName: String
//    let storeName: String
//    let storeDescription: String
//
//    var body: some View {
//        NavigationLink(destination: StoreDetailView(store: StoreInfo(
//            id: 1,
//            name: "Store 1",
//            image: "Gift5",
//            description: "We sell pancakes",
//            contactInfo: "+57 123 456 7890",
//            location: CLLocationCoordinate2D(latitude: 4.652236, longitude: -74.126080))
//        )) {
//                VStack(alignment: .leading) {
//                    Image(imageName)
//                        .resizable()
//                        .aspectRatio(contentMode: .fill)
//                        .frame(width: 250.0, height: 120.0)
//                        .clipped()
//
//                    Text(storeName)
//                        .font(Font.custom("Montserrat-SemiBold", size: 18))
//                        .foregroundColor(colorScheme == .dark ? Color.white : Color("GifterTextDefault"))
//                        .padding(.leading, 10)
//                        .padding(.bottom, 0.5)
//
//                    Text(storeDescription)
//                        .font(Font.custom("WorkSans-Regular", size: 14))
//                        .foregroundColor(Color("GifterText80"))
//                        .padding(.leading, 10)
//                        .padding(.bottom, 16)
//                }
//                .background(RoundedRectangle(cornerRadius: 10).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white))
//                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.12), radius: 12, x: 0, y: 2)
//                .padding(.bottom, 24.0)
//                .padding([.top, .leading], 16.0)
//        }
//    }
//}
//
//
//
//struct addToWishlistButtonView: View {
//    @Binding var addedToWishlist: Bool
//
//
//    var body: some View {
//        Button {
//            self.addedToWishlist.toggle()
//        } label: {
//            Image("favorite")
//                .resizable()
//                .frame(width: 30.0, height: 30.0)
//                .padding([.top, .trailing], 8)
//                .foregroundColor(addedToWishlist ? Color("GifterRed") : Color("GifterGray"))
//        }
//    }
//}
//
//struct WaterfallGiftGrid: View {
//    @EnvironmentObject var giftViewModel: GiftViewModel
//    var scrollingDirection: Axis.Set = .vertical
//    var columns: Int = 2
//    var body: some View {
//        WaterfallGrid(giftViewModel.gifts) { gift in
//            GiftView(gift: gift)
//        }
//        .gridStyle(columns: columns, animation: .easeInOut(duration: 0.5))
//        .padding([.leading, .trailing], 16)
//        .scrollOptions(direction: scrollingDirection)
//    }
//}
//
//
//
