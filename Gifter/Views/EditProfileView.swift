//
//  EditProfileView.swift
//  Gifter
//
//  Created by Sofía Álvarez on 16/04/22.
//

import Amplify
import SwiftUI
import Combine

struct EditProfileView: View {
    
    @Environment(\.colorScheme) var colorScheme
    let user: AuthUser
    @EnvironmentObject var viewModel: UserViewModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @State private var showingAlert = false
    @State private var done = false


    
    private func calculateAge(birthdate: Date) -> Int {
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthdate, to: Date())
        return ageComponents.year!
    }
    
    
    private func isReady(name: String, birthdate: Date) -> Bool {
        return !viewModel.name.isEmpty && (calculateAge(birthdate: viewModel.birthdate) >= 5)
    }
    var body: some View {
            HStack(spacing: 0) {
                Spacer().frame(width: 16, height: 8, alignment: .trailing)
                VStack {
                    GeometryReader { geometry in
                        HStack(spacing: 0) {
                            Button {
                               self.mode.wrappedValue.dismiss()
                            } label: {
                                Image("chevron_left").foregroundColor(Color("GifterRed")).scaledToFit()
                                .transition(.slide)
                            }.buttonStyle(PlainButtonStyle())
                            Text("Edit Profile").font(Font.custom("Montserrat-SemiBold",
                                                         size: 18,
                                                         relativeTo: .title
                                                            ))
                                .fontWeight(.semibold)
                                .foregroundColor(Color("GifterRed"))
                                .multilineTextAlignment(.center)
                                .frame(maxWidth: .infinity,  alignment: .center)
                                .scaledToFit()
                        }
                        VStack(alignment: .leading) {
                            VStack(alignment: .leading){
                                Spacer().frame(height: 40).scaledToFit()
                                (Text("What's your ")
                                        + Text("name")
                                        .foregroundColor(Color("GifterTurquoise"))
                                 + Text("?")).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                                  ))
                                Spacer().frame(height: 16).scaledToFit()
                                TextField(
                                            "Name and last name",
                                            text: $viewModel.name
                                        )
                                .onReceive(Just(viewModel.name)) { inputValue in
                                            if inputValue.count > 30 {
                                                viewModel.name.removeLast()
                                            }
                                        }
                                
                                        .padding()
                                        .font(Font.custom("WorkSans-Regular",
                                                                       size: 14
                                                                         ))
                                        .frame(height: 40)
                                        .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                        .disableAutocorrection(true)
                                HStack {
                                        Spacer().frame(width: 16, height: 4)
                                    Text(viewModel.name.isEmpty ? "The name field is required" : "").foregroundColor(Color("GifterError"))
                                             .font(Font.custom("WorkSans-Regular",
                                                               size: 12
                                                               ))
                               }
                            }
                            VStack(alignment: .leading){
                                Spacer().frame(height: 16).scaledToFit()
                                (Text("Tell others ")
                                        + Text("about you")
                                        .foregroundColor(Color("GifterTurquoise"))
                                 ).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                                  ))
                                Spacer().frame(height: 16).scaledToFit()
                                TextField(
                                            "Write yout bio (Max 40 characters)",
                                            text: $viewModel.bio
                                        )
                                        .onReceive(Just(viewModel.bio)) { inputValue in
                                            if inputValue.count > 40 {
                                                viewModel.bio.removeLast()
                                            }
                                            
                                        }
                                        .padding()
                                        .font(Font.custom("WorkSans-Regular",
                                                                       size: 14
                                                                         ))
                                        .frame(height: 40)
   
                                        .background(RoundedRectangle(cornerRadius: 20).fill(colorScheme == .dark ? Color.ui.darkModeGray : Color.white).shadow(radius: 8))
                                        .disableAutocorrection(true)
                            }
                            VStack(alignment: .leading){
                                Spacer().frame(height: 16)
                                (Text("What's your ")
                                        + Text("birthdate")
                                        .foregroundColor(Color("GifterTurquoise"))
                                 + Text("?")).font(Font.custom("WorkSans-Regular",
                                                                size: 16
                                                              ))
                                Spacer().frame(height: 16)
                                DatePicker(selection: $viewModel.birthdate, in: ...Date(), displayedComponents: .date) {
                                                Text("Select your birthdate").font(Font.custom("WorkSans-Regular",
                                                                               size: 14
                                                                                 ))
                                                    
                                            }//.datePickerStyle(.compact)
                                            .accentColor(Color.ui.red)
                                HStack {
                                        Spacer().frame(width: 16, height: 4)
                                    Text(calculateAge(birthdate: viewModel.birthdate) < 5 ? "You must be at least 5 years old" : "").foregroundColor(Color("GifterError"))
                                             .font(Font.custom("WorkSans-Regular",
                                                               size: 12
                                                               ))
                               }
                                
                                            
                            }
                 
                            GeometryReader { geometry_h in

                            VStack {
                                Spacer().frame(height: (geometry_h.size.height*0.24))
                                HStack{
                                    Spacer().frame(width: geometry_h.size.width*0)
                                Button("Update profile", action: {
                                    if isReady(name: viewModel.name, birthdate: viewModel.birthdate) {
                                        viewModel.update(authUsername: user.username, name: viewModel.name, birthdate: viewModel.birthdate, bio: viewModel.bio)
                                        
                                        done = true
                                    }
                                    else {
                                        showingAlert = true
                                    }
                                    if viewModel.error != "" {
                                        showingAlert = true
                                    }
                                })
                                .foregroundColor(isReady(name: viewModel.name, birthdate: viewModel.birthdate) ? .white : .black)
                                    .buttonStyle(StyledButton())
                                    .background(RoundedRectangle(cornerRadius: 20, style:   .circular).fill(isReady(name: viewModel.name, birthdate: viewModel.birthdate) ? Color.ui.red : Color.ui.disabledGray))
                                    .font(Font.custom("WorkSans-Regular",
                                                      size: 16,
                                                      relativeTo: .title
                                                      ))
                                    .alert(isPresented: $showingAlert) {
                                    
                                        Alert(title: Text("Edit profile Error"), message: viewModel.error != "" ? Text(viewModel.error) : Text("Introduce a valid name and/or birthdate") , dismissButton: Alert.Button.cancel(
                                            Text("OK"), action: {
                                                viewModel.error = ""
                                            }
                                        ))
                                    }
                                    .alert(isPresented: $done) {
                                    
                                        Alert(title: Text("Profile edition"), message: Text("Profile updated successfully"), dismissButton: Alert.Button.cancel(
                                            Text("OK"), action: {
                                             self.mode.wrappedValue.dismiss()
                                            }
                                        ))
                                    }
                                   
                                    
                                }.frame(maxWidth: .infinity, alignment: .center)
                       
      
                            }
                            .buttonStyle(.bordered)
                            }
                        }
                        
                         
                    }
                }
                Spacer().frame(width: 24, height: 8)
            }.onAppear{
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                viewModel.getUser(authUsername: user.username)
                }
            }
            .onDisappear{
               
            }


        .onTapGesture {
            let keyWindow = UIApplication.shared.connectedScenes
                               .filter({$0.activationState == .foregroundActive})
                               .map({$0 as? UIWindowScene})
                               .compactMap({$0})
                               .first?.windows
                               .filter({$0.isKeyWindow}).first
            keyWindow!.endEditing(true)
        }
    }
}


