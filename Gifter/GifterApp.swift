//
//  GifterApp.swift
//  Gifter
//
//  Created by Sofía Álvarez on 24/03/22.
//
import Amplify
import AmplifyPlugins
import SwiftUI

@main
struct GifterApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    // So that we can find out whenever it changes
    @ObservedObject var sessionManager = SessionManager()
    @ObservedObject var userViewModel = UserViewModel()
    @Environment(\.colorScheme) var systemColorScheme
    @State var myColorScheme: ColorScheme?
    @State private var storedColorScheme = UserDefaults.standard.string(forKey: "ColorScheme")
    
    
    init(){
        _ = Amplify.Hub.listen(to: .dataStore) { event in
            if event.eventName == HubPayload.EventName.DataStore.networkStatus {
                guard let networkStatus = event.data as? NetworkStatusEvent else {
                    print("Failed to cast data as NetworkStatusEvent")
                    return
                }
                print("User receives a network connection status: \(networkStatus.active)")
            }
        }
        configureAmplify()
        sessionManager.getCurrentAuthUser()
        //sessionManager.signOut()
        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = UIColor(Color("GifterRed"))
    }
    
    
    
    
    var body: some Scene {
        WindowGroup {
        switch sessionManager.authState {
            case .initial:
            InitialView()
                .environmentObject(sessionManager)
            case .login:
                LoginView()
                    .environmentObject(sessionManager)
            case .signUp:
            SignUpView()
                    .environmentObject(sessionManager)

            case .categories(let username):
            InterestsView(username: username)
                .environmentObject(sessionManager)
                .environmentObject(userViewModel)
            
            case .confirmCode(let username, let email):
            SignUpConfirmationView(username: username, email: email)
                    .environmentObject(sessionManager)
            case .session(let user):
                GifterTabView(user: user)
                    .environmentObject(sessionManager)

            }
        }
    }
    
    private func configureAmplify() {
        do {
            Amplify.Logging.logLevel = .info
            let dataStorePlugin = AWSDataStorePlugin(modelRegistration: AmplifyModels())
            let apiPlugin = AWSAPIPlugin(modelRegistration: AmplifyModels())
            try Amplify.add(plugin: AWSCognitoAuthPlugin())
            try Amplify.add(plugin: dataStorePlugin)
            try Amplify.add(plugin: apiPlugin)
            try Amplify.add(plugin: AWSS3StoragePlugin())
            try Amplify.configure()
            print("Amplify configured successfully")
        } catch {
            print("Could not initialize Amplify", error)
        }
    }
    
}



extension Color {
    static let ui = Color.UI()
    
    struct UI {
        let red = Color("GifterRed")
        let turquoise = Color("GifterTurqouise")
        let disabledGray = Color("GifterDisabledGray")
        let darkModeGray = Color(red: 49.0/255.0, green: 49.0/255.0, blue: 49.0/255.0)
        let gifterGray = Color("GifterGray")
    }
}

struct StyledButton: ButtonStyle {
   func makeBody(configuration: Configuration) -> some View {
       configuration
           .label
           .padding()
           .frame(width: 264, height: 40)
           
   }
}


