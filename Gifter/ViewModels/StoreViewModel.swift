//
//  StoreViewModel.swift
//  Gifter
//
//  Created by Juliana Prieto Arcila on 6/05/22.
//

import MapKit
import Foundation

class StoreViewModel: ObservableObject {
    @Published var stores: [StoreDTO] = []
    @Published var storeGifts = [Int: [GiftDTO]]()
    
    func getNearbyStores(location: CLLocationCoordinate2D) {
        print("Location provided: Long: \(location.longitude) Lat: \(location.latitude)")
        let json: [String: Any] = [
            "location": [ "latitude": location.longitude, "longitude": location.latitude ]
        ]

        let jsonData = try? JSONSerialization.data(withJSONObject: json)

        // create post request
        let url = URL(string: "https://us-central1-gifter-team26.cloudfunctions.net/getNearbyStores")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        // insert json data to the request
        request.httpBody = jsonData

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let error = error {
                print("Error took place \(error)")
                return
            }
            guard let data = data else {return}
            DispatchQueue.main.async {
                do {
                    let decodedStores = try JSONDecoder().decode([StoreDTO].self, from: data)
                    self.stores = decodedStores
                    print("[StoreViewModel nearby stores] These are the stores: \(decodedStores)")
                } catch let jsonErr{
                    print("[StoreViewModel] There was an error: \(jsonErr)")
               }
            }
        }
        task.resume()
    }
    
    func getStoreGifts(storeId: Int) {
        let postString: String = "storeId=\(storeId)"

        // create post request
        let url = URL(string: "https://us-central1-gifter-team26.cloudfunctions.net/getStoreGifts")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        // insert json data to the request
        request.httpBody = postString.data(using: String.Encoding.utf8);

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {return}
            DispatchQueue.main.async {
                do {
                    let decodedGifts = try JSONDecoder().decode([GiftDTO].self, from: data)
                    print("********-----------*********")
                    print("[StoreViewModel getStoreGifts] Response: \(decodedGifts) and storeId \(storeId)")
                    self.storeGifts[storeId] = decodedGifts
                } catch let jsonErr{
                    print("[StoreViewModel getStoreGifts] There was an error: \(jsonErr) and this is the storeId \(storeId)")

               }
            }
        }
        task.resume()
    }
    
    func getEachStoreGifts(store: StoreDTO) async {
        let giftIds = store.gifts
        var storeGiftsList: [GiftDTO] = []
        
        for giftId in giftIds {
            let fetchedGift = await self.getGift(giftId: giftId)
            storeGiftsList.append(fetchedGift)
        }
        
        print("[StoreViewModel] getEachStoreGifts \(storeGiftsList)")
        
        self.storeGifts[store.id] = storeGiftsList
    }
    
    func getGift(giftId: String) async -> GiftDTO {
        let postString: String = "giftId=\(giftId)"

        // create post request
        let url = URL(string: "https://us-central1-gifter-team26.cloudfunctions.net/getGift")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        var gift: GiftDTO = GiftDTO(
            id: "",
            name: "",
            price: 0,
            image: "",
            description: "",
            categories: []
        )

        // insert json data to the request
        request.httpBody = postString.data(using: String.Encoding.utf8);

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {return}
            DispatchQueue.main.async {
                do {
                    let decodedGift = try JSONDecoder().decode(GiftDTO.self, from: data)
                    print("********-----------*********")
                    print("[GiftViewModel getGift] Response: \(decodedGift) and giftId \(giftId)")
//                    gift = decodedGift
                } catch let jsonErr{
                    print("[GiftViewModel getGift] There was an error: \(jsonErr) and this is the giftId \((giftId))")

               }
            }
        }
        task.resume()
        return gift
    }
}
