//
//  UserViewModel.swift
//  Gifter
//
//  Created by Sofía Álvarez on 26/03/22.
//
import Amplify
import SwiftUI
import Foundation

class UserViewModel: ObservableObject {
        @Published var id = String()
        @Published var username = String()
        @Published var name = String()
        @Published var email = String()
        @Published var bio = String()
        @Published var profilePic = String()
        @Published var coverImg = String()
        @Published var birthdate = Date()
        @Published var age = Int()
        @Published var interests : [String] = []
        @Published var error = String()
    
        
        func createUser(authUsername: String) {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            Amplify.DataStore.query(User.self, where: User.keys.username.eq(authUsername)) { result in
            if let queriedUser = try? result.get() {
                if !queriedUser.isEmpty {
                   print("user already exists", queriedUser)
                    error = "User already exists"
                }
                else {
                    let user = User(
                        name: name,
                        email: email,
                        bio: "This is my bio...",
                        username: username,
                        birthdate: formatter.string(from: birthdate),
                        age: Int(calculateAge(birthdate: birthdate)),
                        profilePic: profilePic,
                        coverImg: coverImg,
                        interests: interests
                    )
                   
                    Amplify.DataStore.save(user) {result in
                        switch result {
                        case .success:
                            print(user)
                            print("User saved successfully")
                        case .failure(let error):
                            print("Failed to save user - \(error)")
                        }
                    }
                }
                }
                else {
                    let user = User(
                        name: name,
                        email: email,
                        bio: "This is my bio...",
                        username: username,
                        birthdate: formatter.string(from: birthdate),
                        age: Int(calculateAge(birthdate: birthdate)),
                        profilePic: profilePic,
                        coverImg: coverImg,
                        interests: interests
                    )
                   
                    Amplify.DataStore.save(user) {result in
                        switch result {
                        case .success:
                            print(user)
                            print("User saved successfully")
                        case .failure(let error):
                            print("Failed to save user - \(error)")
                        }
                        
                    }
             }

        }
    }
         
    func createInterests(authUsername: String, interests: [String]) {
        Amplify.DataStore.query(User.self, where: User.keys.username.eq(authUsername)) { result in
            if var queriedUser = try? result.get() {
                print("queried user", queriedUser)
                queriedUser[0].interests = interests
                Amplify.DataStore.save(queriedUser[0]) { result in
                    switch result {
                    case .success:
                        print("User interests updated successfully")
                    case .failure(let error):
                        print("Failed to save interests - \(error)")
                    }
                    
                }
            } else {
                print("error querying user")
                }
            }
    }
    
    func updateInterests(authUsername: String, interests: [String]) {
        Amplify.DataStore.query(User.self, where: User.keys.username.eq(authUsername)) { result in
            if var queriedUser = try? result.get() {
                print("queried user", queriedUser)
                queriedUser[0].interests!.removeAll()
                queriedUser[0].interests = interests
                
                Amplify.DataStore.save(queriedUser[0]) { result in
                    switch result {
                    case .success:
                        print("User interests updated successfully")
                    case .failure(let error):
                        print("Failed to save interests - \(error)")
                    }
                    
                }
            } else {
                print("error querying user")
                }
            }
    }
    
    
    
    func updateProfilePic(authUsername: String, profilePic: String) {
        Amplify.DataStore.query(User.self, where: User.keys.username.eq(authUsername)) { result in
            if var queriedUser = try? result.get() {
                print("queried user", queriedUser)
                queriedUser[0].profilePic = profilePic
                Amplify.DataStore.save(queriedUser[0]) { result in
                    switch result {
                    case .success:
                        print("User profile pic updated successfully")
                    case .failure(let error):
                        print("Failed to save profile pic - \(error)")
                    }
                    
                }
            } else {
                print("error querying user")
                }
            }
        
    }
    
    func updateCoverImg(authUsername: String, coverImg: String) {
        Amplify.DataStore.query(User.self, where: User.keys.username.eq(authUsername)) { result in
            if var queriedUser = try? result.get() {
                print("queried user", queriedUser)
                queriedUser[0].coverImg = coverImg
                Amplify.DataStore.save(queriedUser[0]) { result in
                    switch result {
                    case .success:
                        print("User cover img updated successfully")
                    case .failure(let error):
                        print("Failed to save cover img - \(error)")
                    }
                    
                }
            } else {
                print("error querying user")
                }
            }
        
    }
    
    func getUser(authUsername: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        Amplify.DataStore.query(User.self, where: User.keys.username.eq(authUsername)) { result in
            if let queriedUser = try? result.get() {
                username = queriedUser[0].username
                name = queriedUser[0].name
                email = queriedUser[0].email
                birthdate = formatter.date(from: queriedUser[0].birthdate)!
                age = queriedUser[0].age
                coverImg = queriedUser[0].coverImg ?? ""
                profilePic = queriedUser[0].profilePic ?? ""
                bio = queriedUser[0].bio ?? ""
                for interest in queriedUser[0].interests! {
                    interests.append(interest ?? "")
                }
            } else {
                print("error querying user")
                }
            }
    }
    
    func update(authUsername: String, name: String, birthdate: Date, bio: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        Amplify.DataStore.query(User.self, where: User.keys.username.eq(authUsername)) { result in
            if var queriedUser = try? result.get() {
                print("queried user", queriedUser)
                queriedUser[0].bio = bio
                queriedUser[0].name = name
                queriedUser[0].birthdate = formatter.string(from: birthdate)
                queriedUser[0].age = calculateAge(birthdate: birthdate)
                Amplify.DataStore.save(queriedUser[0]) { result in
                    switch result {
                    case .success:
                        print("User bio created successfully")
                    case .failure(let error):
                        print("Failed to create bio - \(error)")
                    }
                    
                }
            } else {
                print("error querying user")
                }
            }
    }

    
    func uploadProfilePic(authUsername: String, _ image: Image) {
        
        if profilePic != "" {
            deletePicture(key: profilePic)
        }
        guard let imageData = image.asUIImage().jpegData(compressionQuality: 0.5) else {return}
        let key = UUID().uuidString + ".jpg"
        
        _ = Amplify.Storage.uploadData(key: key, data: imageData) { result in
            switch result {
            case .success:
                print("Uploaded image")
                self.updateProfilePic(authUsername: authUsername, profilePic: key)
                
            case .failure(let error):
                print("Failed to upload - \(error)")
            }
        }
    }
    
    func uploadCoverImg(authUsername: String, _ image: Image) {
        
        if coverImg != "" {
            deletePicture(key: coverImg)
        }
        guard let imageData = image.asUIImage().jpegData(compressionQuality: 0.5) else {return}
        let key = UUID().uuidString + ".jpg"
        
        _ = Amplify.Storage.uploadData(key: key, data: imageData) { result in
            switch result {
            case .success:
                print("Uploaded image")
                self.updateCoverImg(authUsername: authUsername, coverImg: key)
            case .failure(let error):
                print("Failed to upload - \(error)")
            }
        }
    }
    
    func deletePicture(key: String) {
       _ = Amplify.Storage.remove(key: key) { event in
            switch event {
            case let .success(data):
                print("Completed: Deleted \(data)")
            case let .failure(storageError):
                print("Failed: \(storageError.errorDescription). \(storageError.recoverySuggestion)")
            }
        }
    }
        
        

func calculateAge(birthdate: Date) -> Int {
    let calendar = Calendar.current
    let ageComponents = calendar.dateComponents([.year], from: birthdate, to: Date())
    return ageComponents.year!
}
}
