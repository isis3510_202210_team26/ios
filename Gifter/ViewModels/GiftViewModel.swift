//
//  GiftViewModel.swift
//  Gifter
//
//  Created by Sofía Álvarez on 26/03/22.
//
import SwiftUI
import Foundation

class GiftViewModel: ObservableObject {
        @Published var gifts: [GiftDTO] = []
        @Published var giftsByCategory: [GiftDTO] = []
        @Published var currentGift = GiftDTO(
            id: "",
            name: "",
            price: 0,
            image: "",
            description: "",
            categories: []
        )

        func getGifts() {
            guard let url = URL(string: "https://us-central1-gifter-team26.cloudfunctions.net/getRecommendedGifts") else { fatalError("Missing URL") }

            let urlRequest = URLRequest(url: url)

            let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if let error = error {
                    print("Request error: ", error)
                    return
                }

                guard let response = response as? HTTPURLResponse else { return }

                if response.statusCode == 200 {
                    guard let data = data else { return }
                    DispatchQueue.main.async {
                        do {
                            let decodedGifts = try JSONDecoder().decode([GiftDTO].self, from: data)
                            self.gifts = decodedGifts
                        } catch let error {
                            print("Error decoding: ", error)
                        }
                    }
                }
            }

            dataTask.resume()
        }
    
    func getGiftsByCategory(cat: String) {
            /*let json: [String: Any] = [
                "categories": [cat]
            ]*/
            let postString = "categories=[\(cat)]"

            //let jsonData = try? JSONSerialization.data(withJSONObject: json)

            // create post request
            let url = URL(string: "https://us-central1-gifter-team26.cloudfunctions.net/getGiftsByCategory")!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"

            // insert json data to the request
            request.httpBody = postString.data(using: String.Encoding.utf8) //jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                DispatchQueue.main.async {
                    do {
                        let decodedGifts = try JSONDecoder().decode([GiftDTO].self, from: data)
                        print("[GiftDTO] Response: \(decodedGifts[0])")
                        self.giftsByCategory = decodedGifts
                    } catch let jsonErr{
                        print("[GiftDTO] There was an error: \(jsonErr)")
                   }
                }
            }
            task.resume()
        }
}
