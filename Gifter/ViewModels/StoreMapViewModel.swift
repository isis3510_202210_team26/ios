//
//  StoreMapViewModel.swift
//  Gifter
//
//  Created by Juliana Prieto Arcila on 6/05/22.
//

import MapKit
import Foundation

struct EquatableLocation: Equatable {
    var location: CLLocationCoordinate2D
    
    static func == (lhs: EquatableLocation, rhs: EquatableLocation) -> Bool {
        return lhs.location.longitude == rhs.location.longitude && lhs.location.latitude == rhs.location.latitude
    }
}

enum MapDetails {
    static let startingLocation = EquatableLocation(location: CLLocationCoordinate2D(latitude: 4.601639898970091, longitude: -74.06596174150783))
    static let defaultSpan = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
}

final class StoreMapViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {

    @Published var coordinateRegion = MKCoordinateRegion(center: MapDetails.startingLocation.location,
                                                          span: MapDetails.defaultSpan)
    
    @Published var currentUserLocation = MapDetails.startingLocation

    var locationManager: CLLocationManager?

    func checkIfLocationServicesIsEnabled() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager!.delegate = self
        } else {
            print("User did not give permissions")
        }
    }

    private func checkLocationAuthorization() {
        guard let locationManager = locationManager else { return }

        switch locationManager.authorizationStatus {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .restricted:
                print("Location is restricted")
            case .denied:
                print("Location is not enabled. Please enable it")
            case .authorizedAlways, .authorizedWhenInUse:
                coordinateRegion = MKCoordinateRegion(
                    center: locationManager.location!.coordinate,
                    span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
                )
            
            currentUserLocation.location = locationManager.location!.coordinate
            
            @unknown default:
                break
        }
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
}
