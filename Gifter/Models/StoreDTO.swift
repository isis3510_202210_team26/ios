//
//  StoreDTO.swift
//  Gifter
//
//  Created by Juliana Prieto Arcila on 6/05/22.
//

import Foundation
import MapKit

public struct StoreDTO: Identifiable, Decodable {
    public let distance: String
    public let id: Int
    public let location: Location
    public let address: String
    public let images: String
    public let description: String
    public let name: String
    public let website: String
    public let gifts: [String]
    public let phone: String
}

public struct Location: Decodable {
    public let _latitude: Double
    public let _longitude: Double
}
