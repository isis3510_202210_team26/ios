// swiftlint:disable all
import Amplify
import Foundation

public struct GiftDTO: Identifiable, Decodable, Hashable  {
  public let id: String
  public var name: String
  public var price: Int
  public var image: String
  public var description: String
  public var categories: [String]
}
